# Feeeld 3.0 iOS & Android APP
React Native, Expo를 이용하여 구성
Expo를 이용해서 구현 내용을 테스트하고 실행합니다.

## Git global setup
기존에 github을 사용하시는 분들은 반드시 `git config` 세팅 확인 부탁드립니다.

    # 확인
    git config --list

    # setup
    # Your name & email -> Settings 에서 체크
    git config --global user.name "Your name"
    git config --global user.email "Your email"

## Git first clone
가장 처음에는 `git clone`을 이용해 프로젝트를 다운받고,
`npm install`을 통해 필요한 패키지들을 모두 다운로드 받습니다.

    # Git clone
    git clone https://gitlab.com/epplix/feeeld_3.0_app.git

    # Package install
    cd feeeld_3.0_app
    npm install

## Git commit
모든 commit 전에, 개발을 시작하기 전에 `git pull`을 수행합니다.
충돌을 방지하기 위한 작업으로 잊지말고 반드시 실행해 주세요.
혹시 충돌이 발생하면 다른 개발자와 상의 후 진행해 주세요.

    # Commit 전 git pull 수행
    git pull origin master

    git add .
    git commit  # commit message 입력은 Notion의 개발 컨벤션 참고
    git push -u origin master

## 실행
  npm run start