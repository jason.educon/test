import { PixelRatio } from "react-native";

const fontScale = PixelRatio.getFontScale();

const FEEELD_BORDER = "1px solid rgba(112,243,158,1)";
const BORDER_RADIUS = "13px";

export default {
  bgColor: "white",
  blackColor: "#333333",
  grayColor: "#DFDFDF",
  lightGrayColor: "#EAEAEA",
  darkGrayColor: "#999",
  redColor: "#ED4956",
  blueColor: "#3897f0",
  darkBlueColor: "#003569",
  feeeldColor: "#55F3B3",
  feeeldBox: `border: ${FEEELD_BORDER};
              border-radius: ${BORDER_RADIUS};
              `,
  editorBorderColor: "rgba(200,200,200,0.2)",
  h1: `font-family: 'bold';
       font-size: ${(1 / fontScale) * 20}px;
       include-font-padding: false;
       padding: 0px;
      `,
  h2: `font-family: 'medium';
      font-size: ${(1 / fontScale) * 16}px;
      include-font-padding: false;
      padding: 0px;
     `,
  h3: `font-family: 'regular';
     font-size: ${(1 / fontScale) * 14}px;
     include-font-padding: false;
     padding: 0px;
    `,
  h4: `font-family: 'regular';
    font-size: ${(1 / fontScale) * 12}px;
    include-font-padding: false;
    padding: 0px;
   `,
  h5: `font-family: 'regular';
    font-size: ${(1 / fontScale) * 10}px;
    include-font-padding: false;
    padding: 0px;
   `,
};
