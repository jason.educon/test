import React, { useState } from "react";

const useFilterModalInput = (initialValue, limit, initScroll, filter) => {
  const [value, setValue] = useState(initialValue);
  const onChange = text => {
    if (text.length > limit) {
      alert("태그는" + limit + " 자를 넘을 수 없습니다.");
      return;
    }
    if (filter !== undefined) {
      filter(text);
    }
    setValue(text);
    initScroll();
  };
  return { value, onChange, setValue };
};

export default useFilterModalInput;
