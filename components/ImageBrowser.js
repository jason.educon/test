/**
 * ImageBrowser을 구현합니다.
 *
 * 레퍼런스 (해당 라이브러리가 동작하지 않아 수정 구현)
 * - https://github.com/MonstroDev/expo-image-picker-multiple#readme
 *
 *
 * 사용한 라이브러리
 * - expo-media-library ; 유저의 미디어 라이브러리(이미지, 비디오) 접근하게 해준다
 * - expo-permissions ; 유저의 권한을 묻는 기능. 배포시 반드시 메타데이터를 추가해줘야 한다고 함 (문서 참고)
 *
 * [!]
 * - setImage의 setSelected 이후에 prepareCallback에서 selected가 바로 반영되지 않는 것 처럼 보여서,
 * newSelected를 직접 전달 받았다. (원래 코드에선 전달 받지 않고, selected를 그대로 이용)
 *
 * [!] 네비게이션을 쓰는 경우, 컴포넌트가 unmount 되지 않을 수 있다. (따라서 componentWillUnmount를 활용할 수 x)
 *  https://reactnavigation./docs/navigation-lifecycle/ 를 참고해서
 *  useFocusEffect를 활용 => 컴포넌트를 네비게이션에 의해 떠나는 순간을 잡을 수 있음.
 */
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Dimensions,
  ActivityIndicator
} from "react-native";

import { useFocusEffect } from "@react-navigation/native";

const { width } = Dimensions.get("window");

import * as MediaLibrary from "expo-media-library";
import * as Permissions from "expo-permissions";

import ImageTile from "./ImageTile";

export const ImageBrowser = ({
  max,
  loadCount,
  callback, // onSubmit을 했을 때, 정보를 전달 받을 함수
  onChange,
  preloaderComponent,
  emptyStayComponent,
  renderSelectedComponent,
  noCameraPermissionComponent
}) => {
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [hasCameraRollPermission, setHasCameraRollPermission] = useState(null);
  const [photos, setPhotos] = useState([]);
  const [selected, setSelected] = useState([]);
  const [isEmpty, setIsEmpty] = useState(false);
  const [after, setAfter] = useState(null);
  const [hasNextPage, setHasNextPage] = useState(true);

  const numColumns = 4;

  const getPermissionsAsync = async () => {
    const { status: camera } = await Permissions.askAsync(Permissions.CAMERA);
    const { status: cameraRoll } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    setHasCameraPermission(camera === "granted");
    setHasCameraRollPermission(cameraRoll === "granted");
  };

  const processPhotos = data => {
    if (data.totalCount) {
      if (after === data.endCursor) return;
      const uris = data.assets;
      setPhotos([...photos, ...uris]);
      setAfter(data.endCursor);
      setHasNextPage(data.hasNextPage);
    } else {
      setIsEmpty(true);
    }
  };

  const getPhotos = () => {
    const params = {
      first: loadCount || 50,
      assetType: "Photos",
      sortBy: ["creationTime"]
    };
    if (after) params.after = after;
    if (!hasNextPage) return;
    MediaLibrary.getAssetsAsync(params).then(processPhotos);
  };

  useEffect(() => {
    async function getPermissions() {
      await getPermissionsAsync();
    }
    getPermissions();
    getPhotos();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      // Do something when the screen is focused
      return () => {
        // Do something when the screen is unfocused
        setSelected([]);
      };
    }, [])
  );

  const getItemLayout = (data, index) => {
    const length = width / 4;
    return {
      length,
      offset: length * index,
      index
    };
  };

  const prepareCallback = newSelected => {
    const selectedPhotos = newSelected.map(i => photos[i]);
    const assetsInfoPromiseAll = Promise.all(
      selectedPhotos.map(i => MediaLibrary.getAssetInfoAsync(i))
    );
    callback(assetsInfoPromiseAll);
  };

  const selectImage = index => {
    let newSelected = Array.from(selected);
    if (newSelected.indexOf(index) === -1) {
      newSelected.push(index);
    } else {
      const deleteIndex = newSelected.indexOf(index);
      newSelected.splice(deleteIndex, 1);
    }
    if (newSelected.length > max) return;
    if (!newSelected) newSelected = [];
    setSelected(newSelected);
    onChange(newSelected.length, () => prepareCallback(newSelected));
  };

  const renderImageTile = ({ item, index }) => {
    const _selected = selected.indexOf(index) !== -1;
    const selectedItemNumber = selected.indexOf(index) + 1;
    return (
      <ImageTile
        item={item}
        index={index}
        selected={_selected}
        selectImage={selectImage}
        selectedItemNumber={selectedItemNumber}
        renderSelectedComponent={renderSelectedComponent}
      />
    );
  };

  const renderPreloader = () =>
    preloaderComponent || <ActivityIndicator size="large" />;
  const renderEmptyStay = () => emptyStayComponent || null;

  const renderImages = () => {
    return (
      <FlatList
        data={photos}
        numColumns={numColumns}
        key={numColumns}
        renderItem={renderImageTile}
        keyExtractor={(_, index) => index}
        onEndReached={() => {
          getPhotos();
        }}
        onEndReachedThreshold={0.5}
        ListEmptyComponent={isEmpty ? renderEmptyStay() : renderPreloader()}
        initialNumToRender={24}
        getItemLayout={getItemLayout}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  if (!hasCameraPermission) {
    return noCameraPermissionComponent || null;
  }

  if (!hasCameraRollPermission) {
    return noCameraPermissionComponent || null;
  }

  return <View style={{ flex: 1 }}>{renderImages()}</View>;
};
