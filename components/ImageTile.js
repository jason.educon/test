import React from "react";

import {
  Dimensions,
  ImageBackground,
  TouchableHighlight,
  View,
} from "react-native";

const { width } = Dimensions.get("window");

export default ({
  item,
  index,
  selected,
  selectImage,
  selectedItemNumber,
  renderSelectedComponent,
}) => {
  if (!item) return null;
  return (
    <TouchableHighlight
      style={{ opacity: selected ? 0.7 : 1 }}
      underlayColor="transparent"
      onPress={() => selectImage(index)}
    >
      <View style={{ position: "relative" }}>
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <ImageBackground
            style={{
              width: width / 3 - 0.5,
              height: width / 3 - 0.5,
              boxSizing: "border-box",
              borderWidth: 1,
              borderColor: "#fff",
            }}
            source={{ uri: item.uri }}
          >
            {selected &&
              renderSelectedComponent &&
              renderSelectedComponent(selectedItemNumber)}
          </ImageBackground>
        </View>
      </View>
    </TouchableHighlight>
  );
};
