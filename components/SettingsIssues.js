/**
 * [SettingsIssues.js]
 * - '설정 > 오류 신고 및 문제 해결' 페이지
 * - 오류 신고 및 문제 해결 관련 이메일 전송 포맷
 */

import React, { useState, useRef } from "react";
import {
  Text,
  Button,
  View,
  ImageBackground,
  Alert,
  KeyboardAvoidingView,
  TouchableWithoutFeedback
} from "react-native";

import { Feather, FontAwesome } from "@expo/vector-icons";
import { setNavHeaderInSettings } from "../utils/navHeader";

import useInput from "../hooks/useInput";

import { useHeaderHeight } from "@react-navigation/stack";
import DraggableFlatList from "react-native-draggable-flatlist";
import Modal from "react-native-modal";

import {
  FloatingButtonContainer,
  Touchable
} from "./GlobalStyledComponent";

import FloatingButton from "./FloatingButton"

import constants from "../constants";

import styled from "styled-components";
import GlobalStyles from "../styles";

const Border = styled.View`
  border-bottom-color: ${GlobalStyles.editorBorderColor};
  border-bottom-width: 1px;
  margin: 15px;
`;

const TitleContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 0 20px;
  margin-bottom: 5px;
`;

const DescriptionContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 0 20px;
  margin-bottom: 5px;
`;

const DescriptionInput = styled.TextInput`
  flex: 1;
  ${GlobalStyles.h2};
  margin-top: 10px;
  margin-left: 5px;
  padding: 5px;
  min-height: 300px;
  height: auto;
  max-height: 500px;
  text-align-vertical: center;
`;

const TitleTextArea = styled.TextInput`
  ${GlobalStyles.h1};
  flex: 1;
  margin-top: 20px;
  margin-left: 5px;
  padding: 5px;
  height: 50px;
  text-align-vertical: center;
`;



export default ({ navigation }) => {
  //setNavHeaderInSettings({ navigation, title: `오류 신고 및 문제 해결` });
  const [title, setTitle] = useState();
  const [inputActivateA, setInputActivateA] = useState();
  const [inputActivateB, setInputActivateB] = useState();
  const [inputActivateC, setInputActivateC] = useState();
  const [content, setContent] = useState();

  const onInputActivateA = () => {
    setInputActivateA(!inputActivateA);
  };
  const onInputActivateB = () => {
    setInputActivateB(!inputActivateB);
  };

  return (
    <FloatingButtonContainer>
      
        <TitleContainer>
          <TitleTextArea
            value={title}
            placeholder="제목을 입력해 주세요"
            placeholderTextColor="rgba(200,200,200,0.7)"
            scrollEnabled={false}
            clearButtonMode="always"
            selectionColor={GlobalStyles.feeeldColor}
            onChangeText={setTitle}
            onFocus={onInputActivateA}
            onBlur={onInputActivateA}
            style={[
              inputActivateA
                ? { borderWidth: 1, borderColor: "#55F3B3", borderRadius: 5 }
                : { backgroundColor: "white" },
            ]}
          />
        </TitleContainer>
        <Border></Border>
        <DescriptionContainer>
          <DescriptionInput
            value={content}
            placeholder="내용을 입력해 주세요"
            placeholderTextColor="rgba(200,200,200,0.7)"
            selectionColor={GlobalStyles.feeeldColor}
            multiline={true}
            onChangeText={setContent}
            onFocus={onInputActivateB}
            onBlur={onInputActivateB}
            style={[
              inputActivateB
                ? { borderWidth: 1, borderColor: "#55F3B3", borderRadius: 5 }
                : { backgroundColor: "white" },
            ]}
          />
        </DescriptionContainer>
        <Border></Border>

        <FloatingButton
        icon="left"
        onPress={() => navigation.goBack()}
        direction="left"
        visible={true}
      />

    </FloatingButtonContainer>
  );
};
