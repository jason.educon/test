/**
 * 에디터에 들어가는, 텍스트 그룹 모듈입니다.
 * 소제목, 본문을 작성할 수 있는 입력폼으로 구성됩니다.
 * 소제목엔 기본적으로 질문이 채워집니다.
 */
import React, { useState, useRef } from "react";
import {
  Text,
  Alert,
  KeyboardAvoidingView,
  TouchableWithoutFeedback
} from "react-native";

import Modal from "react-native-modal";

import styled from "styled-components";
import GlobalStyles from "../styles";
import { FontAwesome, MaterialCommunityIcons } from "@expo/vector-icons";
import { Entypo, AntDesign } from "@expo/vector-icons";

import { Touchable } from "./GlobalStyledComponent";
import PropTypes from "prop-types";
import constants from "../constants";

const Container = styled.View`
  border-bottom-color: ${GlobalStyles.editorBorderColor};
  border-bottom-width: 1px;
  margin: 20px 15px 0px 15px;
  padding-bottom: 25px;
  height: 300px;
  flex-direction: row;
`;

const SubTextContainer = styled.View`
  flex: 8;
  align-items: flex-start;
  margin: 5px;
  margin-top: 0px;
  margin-bottom: 0px;
`;

const SubTitle = styled.Text`
  height: auto;
  width: 100%;
  font-size: 18px;
  line-height: 26px;
  font-family: "medium";
  color: ${GlobalStyles.blackColor};
  padding-bottom: 10px;
  text-align-vertical: top;
  include-font-padding: false;
`;

const TextArea = styled.TextInput`
  flex: 1;
  align-items: flex-start;
  text-align-vertical: top;
  width: 100%;
  font-family: "regular";
  font-size: 16px;
  line-height: 24px;
  include-font-padding: false;
`;

const ControlContainer = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin: 0px;
  padding: 0px;
`;

const ShuffleBtnContainer = styled.View``;

const ContentCounter = styled.Text`
  align-self: flex-end;
  font-size: 10px;
  text-align: right;
  margin-right: 10px;
  color: ${GlobalStyles.darkGrayColor};
  font-family: "regular";
`;

const DeleteBtnContainer = styled.View``;

const ModalView = styled.View`
  margin: 10px;
  padding: 10px;
  background-color: white;
  border-radius: 40px;
  height: 50px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const ModalTitleInput = styled.TextInput`
  flex: 5;
  padding: 0;
  padding-left: 5px;
  margin-right: 5px;
  font-family: "regular";
`;

const ModalTitleCounter = styled.Text`
  margin-right: 5px;
  color: ${GlobalStyles.grayColor};
  font-family: "regular";
`;

const ModalTitleSubmitBtn = styled.View`
  background-color: ${GlobalStyles.feeeldColor};
  border-radius: 20px;
  color: white;
  width: 40px;
  height: 30px;
  padding: 5px;
  margin-left: 5px;
  align-items: center;
  justify-content: center;
`;

const TextGroup = ({
  index,
  textGroupInfoList,
  setTextGroupInfoList,
  changeQuestion,
  removeTextGroup
}) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [modTitleIdx, setModTitleIdx] = useState(0);

  const inputRef = useRef(null);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const modifyTitle = index => {
    setModalVisible(true);
    setModTitleIdx(index);
  };

  const deleteAllTitle = index => {
    textGroupInfoList[index].title = "";
    setTextGroupInfoList(textGroupInfoList.slice());
    inputRef.current.focus();
  };

  const onChangeTitle = value => {
    textGroupInfoList[index].title = value;
    setTextGroupInfoList(textGroupInfoList.slice());
  };

  const onChangeContent = value => {
    textGroupInfoList[index].content = value;
    setTextGroupInfoList(textGroupInfoList.slice());
  };

  const changeTitle = () => {
    const oldTitle = textGroupInfoList[index].title;
    let newTitle = oldTitle;
    do {
      changeQuestion(index);
      newTitle = textGroupInfoList[index].title;
    } while (oldTitle == newTitle);
  };

  const removeTextGroupItem = () => {
    Alert.alert(
      "작성 내용 삭제",
      "선택한 내용을 삭제하시겠습니까?",
      [
        {
          text: "취소"
        },
        {
          text: "삭제",
          onPress: () => {
            removeTextGroup(index);
          }
        }
      ],
      { cancelable: false }
    );
  };

  return (
    <Container>
      <SubTextContainer>
        <TouchableWithoutFeedback onPress={() => modifyTitle(index)}>
          <SubTitle>{textGroupInfoList[index].title}</SubTitle>
        </TouchableWithoutFeedback>

        <TextArea
          placeholder="답변을 입력해주세요."
          value={textGroupInfoList[index].content}
          placeholderTextColor="rgba(200,200,200,0.7)"
          numberOfLines={10}
          multiline={true}
          maxLength={300}
          onChangeText={onChangeContent}
        />
        <ContentCounter>
          {textGroupInfoList[index].content.length}/300
        </ContentCounter>
      </SubTextContainer>

      <ControlContainer>
        <ShuffleBtnContainer>
          <Touchable onPress={changeTitle}>
            <AntDesign name="play" size={30} color={GlobalStyles.feeeldColor} />
          </Touchable>
        </ShuffleBtnContainer>
        <DeleteBtnContainer>
          <Touchable onPress={removeTextGroupItem}>
            <AntDesign
              name="closecircle"
              size={30}
              color={GlobalStyles.feeeldColor}
            />
          </Touchable>
        </DeleteBtnContainer>
      </ControlContainer>

      <Modal
        isVisible={isModalVisible}
        propagateSwipe={true}
        onSwipeComplete={toggleModal}
        onBackdropPress={toggleModal}
        swipeDirection={["left", "right", "down"]}
        backdropOpacity={0.3}
        style={{ justifyContent: "flex-end", margin: 0 }}
      >
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
        >
          <ModalView>
            <ModalTitleInput
              onChangeText={onChangeTitle}
              value={textGroupInfoList[modTitleIdx].title}
              placeholderTextColor={GlobalStyles.lightGrayColor}
              placeholder=""
              autoFocus={true}
              maxLength={30}
              ref={inputRef}
            />
            <TouchableWithoutFeedback
              onPress={() => deleteAllTitle(modTitleIdx)}
            >
              <Entypo
                name="erase"
                size={24}
                color={GlobalStyles.lightGrayColor}
              />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
              <ModalTitleSubmitBtn>
                <Text style={{ fontFamily: "medium" }}>입력</Text>
              </ModalTitleSubmitBtn>
            </TouchableWithoutFeedback>
          </ModalView>
        </KeyboardAvoidingView>
      </Modal>
    </Container>
  );
};

export default TextGroup;
