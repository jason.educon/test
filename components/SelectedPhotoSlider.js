/**
 * screens/Tabs/Editor.js에 포함 되어 선택된 썸네일을 보여준다.
 *
 */
import React from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from "react-native";

import * as Device from "expo-device";

const thumbSize = Device.deviceName.includes("iPad")
  ? {
      height: 275 / 2,
      width: 431 / 2,
      paddingRight: 20 / 2,
    }
  : {
      height: 170 / 1.8,
      width: 305 / 2,
      paddingRight: 25 / 2,
    };

const onScroll = ({ nativeEvent }) => {
  // console.log(nativeEvent);
};

const SelectedPhotoSlider = ({ photos }) => {
  return (
    <View style={styles.container}>
      <ScrollView
        horizontal={true}
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}
        style={styles.scrollView}
        onScroll={onScroll}
      >
        {photos.map((photo, i) => (
          <TouchableOpacity
            key={i}
            /* 이미지 클릭 이벤트 비활성화 
            onPress={() => alert("이미지 클릭 이벤트")} 
            */
          >
            <View style={{ flex: 1 }}>
              <View style={{ ...thumbSize }}>
                <View
                  style={{
                    flex: 1,
                    borderColor: "rgba(220,220,220,0.5)",
                    borderWidth: 1,
                    borderRadius: 10,
                  }}
                >
                  <Image
                    style={{ flex: 1, borderRadius: 10 }}
                    source={{
                      uri: photo.uri,
                    }}
                  />
                </View>
              </View>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingLeft: 18,
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  loading: {
    flex: 1,
    alignItems: "center",
  },
});

export default SelectedPhotoSlider;
