/**
 * 포스트 뷰어에 포함되는 댓글 뷰를 구현합니다.
 *
 * todo ; 키보드 클릭시 버튼을 없애기 위해 추가해 놓은 코드는 이후에 모듈화가 필요할 수도
 */
import React, { useState, useEffect } from "react";

import { useQuery } from "react-apollo-hooks";
import { gql } from "apollo-boost";

import { Text, Image, Dimensions, Keyboard } from "react-native";

import { FontAwesome } from "@expo/vector-icons";
import styled from "styled-components";
import GlobalStyles from "../styles";
import HTMLView from "react-native-htmlview";

const { width, height } = Dimensions.get("window");

const GET_COMMENTS = gql`
  query getComments($postId: String!) {
    getComments(postId: $postId) {
      id
      body
      User {
        id
        nick
        profileImage
      }
      CommentId
      createdAt
    }
  }
`;

const CommentContainer = styled.View`
  padding: 15px;
`;

const InputContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 5px;
`;

const InputComment = styled.TextInput`
  width: ${width - 65}px;
  height: 35px;
  border-color: ${GlobalStyles.grayColor};
  border-width: 1px;
  border-radius: 20px;
  padding: 1px 15px;
`;

const InputIcon = styled.View`
  align-self: flex-end;
  padding-bottom: 5px;
  padding-right: 3px;
`;

const ListContainer = styled.View`
  margin: 5px;
  align-items: flex-start;
`;

const ListItem = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 3px 0px;
`;

const ListItemAvatar = styled.View`
  margin-right: 5px;
`;

const ListItemContent = styled.View`
  align-self: flex-start;
  flex: 3;
  margin-left: 3px;
`;

const Nick = styled.Text`
  font-size: 12px;
  color: ${GlobalStyles.blackColor};
  font-family: "regular";
`;

const Cmt = styled.Text`
  font-size: 14px;
  color: ${GlobalStyles.darkGrayColor};
  font-family: "medium";
`;

const Comments = ({ postId, setShowFloatingButton }) => {
  const { loading, data } = useQuery(GET_COMMENTS, {
    variables: { postId },
  });
  const _showFloatingButton = () => {
    setShowFloatingButton(true);
  };

  const _offFloatingButton = () => {
    setShowFloatingButton(false);
  };

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", _offFloatingButton);
    Keyboard.addListener("keyboardDidHide", _showFloatingButton);

    return () => {
      Keyboard.removeListener("keyboardDidShow", _offFloatingButton);
      Keyboard.removeListener("keyboardDidHide", _showFloatingButton);
    };
  }, []);

  return (
    <CommentContainer>
      <InputContainer>
        <InputComment
          placeholder={"댓글로 의견을 나누어 보세요"}
          placeholderTextColor={GlobalStyles.grayColor}
        />
        <InputIcon>
          <FontAwesome name="send" size={24} color={GlobalStyles.feeeldColor} />
        </InputIcon>
      </InputContainer>
      {loading ? (
        <Text>Loading...</Text>
      ) : (
        data &&
        data.getComments && (
          <>
            {data.getComments.map((comment, index) => (
              <ListContainer key={index}>
                <ListItem>
                  <ListItemAvatar>
                    {comment.User.profileImage !== "/user_default.png" ? (
                      <Image
                        style={{ height: 30, width: 30, borderRadius: 50 }}
                        source={{ uri: comment.User.profileImage }}
                      />
                    ) : (
                      <FontAwesome
                        name="user-circle"
                        size={30}
                        color={GlobalStyles.grayColor}
                      />
                    )}
                  </ListItemAvatar>
                  <ListItemContent>
                    <Nick>{comment.User.nick}</Nick>
                    <Cmt>{comment.body}</Cmt>
                  </ListItemContent>
                </ListItem>
              </ListContainer>
            ))}
          </>
        )
      )}
    </CommentContainer>
  );
};

export default Comments;
