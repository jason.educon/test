/**
 * 화면 하단의 플로팅 버튼을 정의합니다.
 * 전달되는 direction 파라미터에 따라서 플로팅 버튼의 위치를 결정합니다.
 *
 * icon은 기본적으로 AntDesign을 사용합니다.
 * - left, right, heart, hearto, ...
 *
 * 예외 케이스로 feeeld를 전달하는 경우 필디 브러쉬 이미지를 사용합니다.
 *
 * visible 파라미터로 버튼의 숨김 여부를 컨트롤 할 수 있습니다.
 * 항상 보이게 하고 싶다면 생성시 visible={true}로 세팅합니다.
 *
 * 사용 예시)
 * * 반드시 flex:1 인 컨테이너로 감싸줍니다!
 * <View style={{ flex: 1 }}>
 *  <Something></Something>
 *  <FloatingButton icon="~~" direction="~~" onPress={} visible={true} />
 * </View>
 */
import React from "react";
import { StyleSheet, Text, Image, TouchableOpacity } from "react-native";

import { AntDesign } from "@expo/vector-icons";

const FloatingButton = ({ icon, direction, onPress, visible }) => {
  let styleByDirection, feeeldBgColor, iconColor;

  if (icon.startsWith("feeeld")) {
    feeeldBgColor = styles.BgColor;
  }

  if (icon == "heart" || icon == "check") {
    feeeldBgColor = styles.BgColor;
    iconColor = "white";
  } else {
    iconColor = "black";
  }

  if (direction == "left") {
    styleByDirection = styles.LeftFloatingButton;
  } else {
    styleByDirection = styles.RightFloatingButton;
  }

  return visible ? (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.Button, styleByDirection, feeeldBgColor]}
    >
      {icon.startsWith("feeeld") ? (
        <Image
          source={require("../assets/brush.png")}
          style={styles.ImageIcon}
        />
      ) : (
        <AntDesign name={icon} size={20} color={iconColor} />
      )}
    </TouchableOpacity>
  ) : (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.TransButton, styleByDirection, feeeldBgColor]}
    >
      {icon.startsWith("feeeld") ? (
        <Image
          source={require("../assets/brush.png")}
          style={styles.ImageIcon}
        />
      ) : (
        <AntDesign name={icon} size={20} color={iconColor} />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Button: {
    position: "absolute",
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    borderRadius: 30,
    shadowOpacity: 0.1,
    shadowOffset: { width: 0, height: 5 },
    shadowColor: "#000000",
    shadowRadius: 3,
  },
  TransButton: {
    position: "absolute",
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(227,224,223,0.6)",
    borderRadius: 30,
  },
  BgColor: {
    backgroundColor: "#55F3B3",
  },
  RightFloatingButton: {
    right: 25,
    bottom: 30,
  },
  LeftFloatingButton: {
    left: 25,
    bottom: 30
  },
  ImageIcon: {
    width: 45,
    height: 45,
  },
});

export default FloatingButton;
