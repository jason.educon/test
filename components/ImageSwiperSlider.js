/**
 * Post.js(포스트 뷰어)에 들어가는 이미지 스와이퍼 및 보조 이미지 슬라이더를 구현
 * - Swiper ; 한 장씩 넘기는 뷰
 * - Slider ; 여러 장의 썸네일을 넘기는 뷰, 실제로는 ScrollView를 이용해 구현
 */
import React, { useState, useRef, forwardRef } from "react";

import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  Modal,
} from "react-native";

import styled from "styled-components";
import Swiper from "react-native-swiper";
// import ImageZoomViewer from "react-native-image-viewing";
import ImageViewer from "react-native-image-zoom-viewer";

import { Entypo } from "@expo/vector-icons";

import getResizedImageUrl from "../utils/ImageUrl";
import { Touchable } from "./GlobalStyledComponent";
import GlobalStyles from "../styles";

const { width, height } = Dimensions.get("window");

const swiperImageSize = {
  height: 280,
};
const sliderImageSize = {
  width: 63,
  height: 40,
};

const contentPadding = 15;

const thumbCountInWindow = Math.floor(
  (width - contentPadding * 2) / (sliderImageSize.width + contentPadding)
);
const slidingSize =
  (sliderImageSize.width + contentPadding) * thumbCountInWindow;

const Container = styled.View`
  background-color: white;
`;

const TouchableSwiperItem = styled.TouchableOpacity`
  height: ${swiperImageSize.height}px;
  justify-content: center;
`;

const SwiperView = forwardRef(({ imgList, onNext, setZoomMode }, ref) => {
  return (
    <Swiper
      showsPagination={false}
      style={{ height: swiperImageSize.height }}
      onIndexChanged={onNext}
      loop={false}
      ref={ref}
    >
      {imgList.map((uri, i) => (
        <TouchableSwiperItem
          key={i}
          onPress={() => setZoomMode(true)}
          activeOpacity={1}
        >
          <Image
            style={{ flex: 1, resizeMode: "contain" }}
            source={{
              uri: getResizedImageUrl({
                uri,
                width: 0,
                height: swiperImageSize.height * 2,
              }),
            }}
          />
        </TouchableSwiperItem>
      ))}
    </Swiper>
  );
});

const styles = StyleSheet.create({
  selectedSliderItem: {
    borderWidth: 2,
    borderColor: GlobalStyles.feeeldColor,
  },
});

const TouchableSliderItem = styled.TouchableOpacity`
  margin-right: ${contentPadding}px;
`;

const SliderView = forwardRef(
  ({ imgList, curImgIdx, onClickSliderItem }, ref) => {
    return (
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={{ margin: contentPadding, height: sliderImageSize.height }}
        ref={ref}
      >
        {imgList.map((uri, i) => (
          <TouchableSliderItem key={i} onPress={() => onClickSliderItem(i)}>
            <Image
              style={[
                {
                  flex: 1,
                  width: sliderImageSize.width,
                  borderWidth: 1,
                  borderColor: "rgba(200,200,200,0.5)",
                  borderRadius: 5,
                },
                curImgIdx == i ? styles.selectedSliderItem : undefined,
              ]}
              source={{
                uri: getResizedImageUrl({
                  uri,
                  width: 0,
                  height: sliderImageSize.height * 2,
                }),
              }}
            />
          </TouchableSliderItem>
        ))}
      </ScrollView>
    );
  }
);

const ZoomViewerFooterView = styled.View`
  height: 64px;
  align-items: center;
  justify-content: center;
`;

const ImageSwiperSlider = ({ imgList }) => {
  const [curImgIdx, setCurImgIdx] = useState(0);
  const [zoomMode, setZoomMode] = useState(false);
  const swiperRef = useRef();
  const sliderRef = useRef();

  const alignSlider = (index) => {
    if (index > 0 && index % thumbCountInWindow == 0) {
      const times = index / thumbCountInWindow;
      sliderRef.current.scrollTo({
        x: slidingSize * times,
        y: 0,
        animated: true,
      });
    }
    if (index > 0 && (index + 1) % thumbCountInWindow == 0) {
      const times = (index + 1) / thumbCountInWindow - 1;
      sliderRef.current.scrollTo({
        x: slidingSize * times,
        y: 0,
        animated: true,
      });
    }
  };

  const onNext = (index) => {
    setCurImgIdx(index);
    alignSlider(index);
  };

  const onClickSliderItem = (index) => {
    const relativeOffset = index - curImgIdx;
    swiperRef.current.scrollBy(relativeOffset, true);
  };

  const ZoomViewerFooter = ({ imageIndex }) => {
    return (
      <ZoomViewerFooterView>
        <Text style={{ color: "white" }}>
          {imageIndex + 1} / {imgList.length}
        </Text>
      </ZoomViewerFooterView>
    );
  };

  return (
    <Container>
      <SwiperView
        imgList={imgList}
        onNext={onNext}
        ref={swiperRef}
        setZoomMode={setZoomMode}
      />
      <SliderView
        imgList={imgList}
        curImgIdx={curImgIdx}
        ref={sliderRef}
        onClickSliderItem={onClickSliderItem}
      />
      {/* <ImageZoomViewer
        images={imgList.map((x) => {
          return { uri: x };
        })}
        imageIndex={curImgIdx}
        visible={zoomMode}
        onRequestClose={() => setZoomMode(false)}
        swipeToCloseEnabled={true}
        FooterComponent={ZoomViewerFooter}
        presentationStyle="pageSheet"
      /> */}
      <Modal
        visible={zoomMode}
        transparent={true}
        // onRequestClose={() => setZoomMode(false)}
      >
        <ImageViewer
          imageUrls={imgList.map((x) => {
            return { url: x };
          })}
          index={curImgIdx}
          onCancel={() => setZoomMode(false)}
          onClick={() => setZoomMode(false)}
          menus={() => {}}
          saveToLocalByLongPress={false}
          enableSwipeDown={true}
        ></ImageViewer>
      </Modal>
    </Container>
  );
};

export default ImageSwiperSlider;
