/**
 * 포트스 뷰어를 구현
 * ImageSwiperSlider, PostBottmModal, Comment 컴포넌트 등을 포함
 *
 * [!] setShowFloatingButton은 포스트 뷰어 컴포넌트에서 쓰는게 아닌, 자식 컴포넌트(Comemnts.js)에서
 * 댓글 입력시 하단 버튼을 숨기기 위한 것.
 */
import React, { useState } from "react";

import { Text, Image, View, ScrollView, StyleSheet } from "react-native";

import { Entypo, FontAwesome } from "@expo/vector-icons";
import styled from "styled-components";
import GlobalStyles from "../styles";

import HTMLView from "react-native-htmlview";

import Comments from "./Comments";
import PostBottomModal from "./PostBottomModal";
import ImageSwiperSlider from "./ImageSwiperSlider";

const Container = styled.View`
  margin-bottom: 40px;
  background-color: white;
`;

const Header = styled.View`
  padding: 15px;
`;

const HeaderUserContainer = styled.View`
  margin-left: 5px;
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const HeaderUser = styled.View`
  align-items: center;
  flex-direction: row;
  justify-content: center;
`;

const HeaderDot = styled.View`
  align-self: flex-end;
`;

const Touchable = styled.TouchableOpacity``;

const Bold = styled.Text`
  ${GlobalStyles.h1};
`;

const Nick = styled.Text`
  ${GlobalStyles.h2};
  color: ${GlobalStyles.darkGrayColor};
  padding: 3px;
`;

const Follow = styled.Text`
  ${GlobalStyles.feeeldBox};
  font-size: 12px;
  color: ${GlobalStyles.feeeldColor};
  padding: 0 10px;
  text-align: center;
  margin-left: 15px;
  font-family: "medium";
`;

const IconContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 5px;
  margin-left: 15px;
`;

const IconLeftContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

const IconRightContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  margin-right: 15px;
`;

const NumberText = styled.Text`
  ${GlobalStyles.h3};
  color: ${GlobalStyles.blackColor};
  padding: 3px;
  margin-left: 3px;
  margin-right: 8px;
`;

const DetailText = styled.Text`
  font-size: 14px;
  color: ${GlobalStyles.feeeldColor};
  padding: 3px;
  margin-left: 15px;
  margin-bottom: 5px;
  font-family: "regular";
`;

const Line = styled.View`
  border-color: ${GlobalStyles.grayColor};
  border-width: 0px;
  border-top-width: 0.5px;
  margin: 1px 15px;
`;

const Content = styled.View`
  margin: 5px 15px;
  padding: 3px;
  font-family: "regular";
`;

const ContentText = styled.Text`
  ${GlobalStyles.h3};
`;

const TagContainer = styled.View`
  flex-direction: row;
  padding: 5px 15px;
`;

const TagText = styled.Text`
  ${GlobalStyles.h3};
  ${GlobalStyles.feeeldBox};
  color: ${GlobalStyles.darkGrayColor};
  padding: 0 15px;
  text-align: center;
  margin-left: 3px;
  margin-right: 3px;
`;

const Post = ({
  postId,
  title,
  content,
  imgList,
  mainImgIdx,
  Hashtags,
  Likes,
  viewCount,
  User,
  //version,
  setShowFloatingButton,
  // navigation,
}) => {
  const [visibleModal, setVisibleModal] = useState(false);
  let version = 2;

  const closeModal = () => {
    setVisibleModal(false);
  };

  const contentHTML = `<p>` + content + `</p`;
  const likeCount = Likes.length;
  const profileImg = User.profileImage;
  const isUserProfile = profileImg !== "/user_default.png";
  const thumbnail = imgList[mainImgIdx];
  const detailedPost = {
    title: title,
    content: content,
    thumbnail: thumbnail,
    hashtags: Hashtags,
    user: User,
    viewCount: viewCount,
    likeCount: likeCount,
  };

  return (
    <Container>
      <Header>
        <Bold>{title.trim()}</Bold>
        <HeaderUserContainer>
          <HeaderUser>
            <Touchable>
              {isUserProfile ? (
                <Image
                  style={{ height: 24, width: 24, borderRadius: 50 }}
                  source={{ uri: profileImg }}
                />
              ) : (
                <FontAwesome
                  name="user-circle"
                  size={24}
                  color={GlobalStyles.grayColor}
                />
              )}
            </Touchable>
            <Touchable>
              <Nick>{User.nick}</Nick>
            </Touchable>
            {/* <Touchable>
              <Follow>팔로우</Follow>
            </Touchable> */}
          </HeaderUser>

          {/* <HeaderDot>
            <Touchable>
              <Entypo name="dots-three-vertical" size={20} />
            </Touchable>
          </HeaderDot> */}
        </HeaderUserContainer>
      </Header>

      <ImageSwiperSlider imgList={imgList} />

      <IconContainer>
        <IconLeftContainer>
          <Touchable>
            <Entypo color={GlobalStyles.feeeldColor} size={20} name="heart" />
          </Touchable>
          <NumberText>{likeCount}</NumberText>
          <Touchable>
            <Entypo color={GlobalStyles.grayColor} size={20} name="eye" />
          </Touchable>
          <NumberText>{viewCount}</NumberText>
        </IconLeftContainer>
        {/* <IconRightContainer>
          <Entypo color={GlobalStyles.grayColor} size={20} name="share" />
          <NumberText></NumberText>
          <Touchable>
            <Entypo color={GlobalStyles.grayColor} size={20} name="folder" />
          </Touchable>
        </IconRightContainer> */}
      </IconContainer>

      {/* <Touchable onPress={() => setVisibleModal(true)}>
        <DetailText>작품 상세 정보</DetailText>
      </Touchable> */}

      <Line />

      <Content>
        {content.trim().length > 0 ? (
          <HTMLView value={contentHTML} stylesheet={styles} />
        ) : (
          <Text style={{ fontFamily: "regular" }}>내용이 없습니다.</Text>
        )}
      </Content>

      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View flex={1} onStartShouldSetResponder={() => true}>
          <TagContainer>
            {Hashtags.map((hashtag, index) => (
              <TagText key={index}>{hashtag.title}</TagText>
            ))}
          </TagContainer>
        </View>
      </ScrollView>

      <Line />

      {version == 2 ? (
        <View></View>
      ) : (
        <Comments
          postId={postId}
          setShowFloatingButton={setShowFloatingButton}
        />
      )}

      {/* <PostBottomModal
        visibleModal={visibleModal}
        closeModal={closeModal}
        post={detailedPost}
      /> */}
    </Container>
  );
};

const styles = StyleSheet.create({
  p: {
    fontFamily: "regular",
    textAlign: "justify",
  },
});

export default Post;
