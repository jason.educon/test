/**
 * 작품 업로드 시 메타 데이터를 입력하는 에디터입니다.
 * 탭 형식으로 구성되어 있습니다.
 * - 저작권 정보 입력 탭(Copyright.js)
 * - 작품 상세 정보 입력 탭(DetailedInfo.js)
 */
import React, { useState, Component, useRef} from "react";
import { View, Text, Animated } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import Terms1 from "./Terms1";
import Terms2 from "./Terms2";

import GlobalStyle from "../styles";
import { setNavHeader } from "../utils/navHeader";

import { FloatingButtonContainer } from "./GlobalStyledComponent";
import FloatingButton from "./FloatingButton";

import {MaterialCommunityIcons } from "@expo/vector-icons"; //swipe 이미지

const FadeInImage = (props) => {
    const fadeAnim = useRef(new Animated.Value(1)).current  // 불투명도:1
  
    React.useEffect(() => {
      Animated.timing(
        fadeAnim,
        {
          toValue: 0, //불투명도:0
          duration: 3000, //3000ms
          useNativeDriver: false
        }
      ).start();
    }, [fadeAnim])
  
    return (
      <Animated.Image                 // Special animatable View
        style={{
          ...props.style,
          opacity: fadeAnim,         // Bind opacity to animated value
        }}
        source = {require("../assets/swipe.png")} //swipe 이미지
      >
      </Animated.Image>
    );
  }

export default ({navigation}) => {
  const Tab = createMaterialTopTabNavigator();
  setNavHeader({ navigation, visible: false });
  const [showFloatingButton, setShowFloatingButton] = useState(true);

  return (
    <FloatingButtonContainer>
    <Tab.Navigator
      initialRouteName="Term1"
      tabBarOptions={{
        labelStyle: { fontFamily: "bold" },
        indicatorStyle: { backgroundColor: GlobalStyle.feeeldColor },
        navigationOptions: {headerMode: "none"},
        style: {backgroundColor: 'white', height: 75, padding:0, margin:0},
        tabStyle: {height: 100}
      }}
    >
      <Tab.Screen name="Terms" component={Terms1} options={{ tabBarLabel: '이용약관' }}/>
      <Tab.Screen name="Privacy" component={Terms2} options={{ tabBarLabel: '개인정보 보호정책' }}/>
    </Tab.Navigator>

    {/* <FloatingButtonTrans
        icon="gesture-swipe-right"
        //onPress={() => navigation.navigate('Privacy')}
        direction="middle-right"
        visible={showFloatingButton}
      />   */}

    {/* <MaterialCommunityIcons style={{ position: "absolute", top:300, left: 170, opacity: 0.6}} name="gesture-swipe-right" size={80} color="lightgreen" /> */}
    
    {/* <ImageLoader
      style={{ position: "absolute", top:300, left: 100, opacity: 0.5, width: 200,
      height: 200}}
      source={require("../assets/swipe.png")}
    /> */}

    <FadeInImage
        style={{ position: "absolute", top:300, left: 100, width: 200,
        height: 200}}
        source={require("../assets/swipe.png")}
      />
    
    <FloatingButton
        icon="left"
        onPress={() => navigation.goBack()}
        direction="left"
        visible={false}
      />  
    </FloatingButtonContainer>

  );
};
