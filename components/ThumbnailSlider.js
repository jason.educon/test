/**
 * screens/Tabs/Home.js에 포함 되어 썸네일을 보여준다.
 * [!] navigation을 전달 받아 Viewer로 라우팅 해준다. navigation을 누가 전달해주는지 확인 필요.
 * (NavController에 의해 라우팅되는 요소들에게 자동으로 추가되는 건가?)
 */
import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from "react-native";

import PropTypes from "prop-types";
import * as Device from "expo-device";

import getResizedImageUrl from "../utils/ImageUrl";

import styled from "styled-components";
import GlobalStyles from "../styles";
import { FontAwesome } from "@expo/vector-icons";

const ContainerView = styled.View`
  padding-top: 10px;
  padding-left: 18px;
  flex: 1;
`;

const Title = styled.Text`
  ${GlobalStyles.h2};
`;

const SliderView = styled.ScrollView`
  flex: 1;
  margin-top: 10px;
`;

const InfoContainer = styled.View`
  margin-top: 3px;
  flex-direction: row;
  align-items: center;
`;

const ProfileImage = styled.Image`
  height: 18px;
  width: 18px;
  border-radius: 9px;
  margin-right: 3px;
`;

const Nick = styled.Text`
  ${GlobalStyles.h4};
  margin-right: 5px;
`;

const Version = styled.Text`
  ${GlobalStyles.h5};
  padding: 0 5px;
  border-radius: 5px;
  border-color: ${GlobalStyles.feeeldColor};
  background-color: ${GlobalStyles.feeeldColor};
  color: white;
  text-align: center;
  text-align-vertical: center;
`;

const CountView = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;

const ViewCount = styled.Text`
  ${GlobalStyles.h5};
  text-align: right;
  margin-left: 3px;
`;

const thumbSizeByMode = Device.deviceName.includes("iPad")
  ? {
      "1+": {
        height: 275,
        width: 431,
        marginRight: 20,
      },
      "2+": {
        height: 230,
        width: 358,
        marginRight: 20,
      },
    }
  : {
      "1+": {
        height: 200,
        width: 305,
        marginRight: 25,
      },
      "2+": {
        height: 100,
        width: 160,
        marginRight: 10,
      },
    };

const onScroll = ({ nativeEvent }) => {
  // console.log(nativeEvent);
};

const ThumbnailSlider = ({ navigation, title, mode, posts }) => {
  return (
    <ContainerView>
      <Title>{title}</Title>
      <SliderView
        horizontal={true}
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}
        onScroll={onScroll}
      >
        {posts.map((post, i) => (
          <TouchableOpacity
            key={i}
            onPress={() => navigation.navigate("Viewer", { postId: post.id })}
          >
            <View style={{ flex: 1 }}>
              <View style={{ ...thumbSizeByMode[mode] }}>
                {/* 이미지 썸네일 */}
                <View
                  style={{
                    flex: 1,
                    borderColor: "rgba(220,220,220,0.5)",
                    borderWidth: 0,
                  }}
                >
                  <Image
                    style={{ flex: 1, borderRadius: 10 }}
                    source={{
                      uri: getResizedImageUrl({
                        uri: post.imgList[0],
                        width: thumbSizeByMode[mode].width,
                        height: 0,
                      }),
                    }}
                  />
                </View>
              </View>

              {/* 썸네일 인포 */}
              <InfoContainer
                style={{
                  width: thumbSizeByMode[mode].width,
                }}
              >
                {post.User.profileImage ? (
                  <ProfileImage
                    source={{
                      uri: getResizedImageUrl({
                        uri: post.User.profileImage,
                        width: 18,
                        height: 18,
                      }),
                    }}
                  />
                ) : (
                  <FontAwesome
                    name="user-circle"
                    size={18}
                    color={styles.grayColor}
                  />
                )}
                <Nick>{post.User.nick}</Nick>
                <Version>{post.version == 2 ? "2.0" : "3.0"}</Version>
                <CountView>
                  <FontAwesome name="heart" size={14} color={"#EAEAEA"} />
                  <ViewCount>{post.Likes.length}</ViewCount>
                </CountView>
              </InfoContainer>
              {/* 썸네일 인포 끝 */}
            </View>
          </TouchableOpacity>
        ))}
      </SliderView>
    </ContainerView>
  );
};

ThumbnailSlider.propTypes = {
  title: PropTypes.string.isRequired,
  mode: PropTypes.oneOf(["1+", "2+"]).isRequired,
};

export default ThumbnailSlider;
