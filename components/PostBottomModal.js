/**
 * 포스트 뷰어의 작품 상세 보기 클릭시 보이는 바텀 모달을 구현
 */
import React, { useState } from "react";

import {
  View,
  Text,
  Image,
  Button,
  ScrollView,
  Platform,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native";

import HTMLView from "react-native-htmlview";
import Modal from "react-native-modal";

import { FontAwesome, Entypo, AntDesign } from "@expo/vector-icons";
import styled from "styled-components";
import GlobalStyles from "../styles";

const { width, height } = Dimensions.get("window");

const Touchable = styled.TouchableOpacity``;

const ModalContainer = styled.View`
  background-color: white;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  padding: 15px;
  border-color: rgba(0, 0, 0, 0.1);
  height: ${height * 0.9}px;
`;

const DetailContainer = styled.View`
  margin-top: 10px;
`;

const DetailHeaderContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 5px;
`;

const DetailHeader = styled.View`
  flex: 3;
  margin: 1px 5px;
  padding: 3px;
  align-self: flex-start;
`;

const DetailHeaderTitle = styled.Text`
  font-size: 18px;
  font-family: "medium";
`;

const IconLeftContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin-top: 10px;
`;

const NumberText = styled.Text`
  font-size: 14px;
  color: ${GlobalStyles.grayColor};
  padding: 3px;
  margin-left: 3px;
  margin-right: 8px;
  font-family: "regular";
`;

const DetailPostContainer = styled.View`
  align-items: flex-start;
  justify-content: space-between;
  margin: 5px 1px;
`;

const DetailPostCategory = styled.View`
  margin: 8px 1px;
`;

const DetailPostTitle = styled.Text`
  font-size: 16px;
  padding: 3px;
  font-family: "medium";
`;

const DetailPostText = styled.Text`
  font-size: 14px;
  padding: 3px;
  font-family: "regular";
`;

const HeaderUser = styled.View`
  align-items: center;
  flex-direction: row;
  justify-content: center;
  margin-top: 3px;
`;

const Nick = styled.Text`
  font-size: 14px;
  color: ${GlobalStyles.darkGrayColor};
  padding: 3px;
  margin-left: 3px;
  font-family: "regular";
`;

const Follow = styled.Text`
  ${GlobalStyles.feeeldBox};
  font-size: 12px;
  color: ${GlobalStyles.feeeldColor};
  padding: 1px 10px;
  text-align: center;
  margin-left: 15px;
  font-family: "regular";
`;

const TagContainer = styled.View`
  flex-direction: row;
  padding: 3px;
`;

const TagText = styled.Text`
  ${GlobalStyles.feeeldBox};
  font-size: 14px;
  color: ${GlobalStyles.darkGrayColor};
  padding: 0 15px;
  text-align: center;
  margin-left: 3px;
  margin-right: 3px;
  font-family: "regular";
`;

const PostBottomModal = ({ visibleModal, closeModal, post }) => {
  const thumb = post.thumbnail;
  const profileImg = post.user.profileImage;
  const isUserProfile = profileImg !== "/user_default.png";

  return (
    <Modal
      propagateSwipe={true}
      isVisible={visibleModal}
      onSwipeComplete={closeModal}
      onBackdropPress={closeModal}
      swipeDirection={["left", "right", "down"]}
      style={{ justifyContent: "flex-end", margin: 0 }}
    >
      <ModalContainer>
        <Touchable onPress={closeModal} style={{ alignSelf: "center" }}>
          <AntDesign name="closecircle" size={24} color={GlobalStyles.feeeldColor} />
        </Touchable>
        <DetailContainer>
          <DetailHeaderContainer>
            <Image
              source={{ uri: thumb }}
              style={{ height: 80, width: 150, borderRadius: 4 }}
            />
            <DetailHeader>
              <DetailHeaderTitle>{post.title}</DetailHeaderTitle>
              <IconLeftContainer>
                <Touchable>
                  <Entypo color={GlobalStyles.feeeldColor} size={20} name="heart" />
                </Touchable>
                <NumberText>{post.likeCount}</NumberText>
                <Touchable>
                  <Entypo color={GlobalStyles.darkGrayColor} size={20} name="eye" />
                </Touchable>
                <NumberText>{post.viewCount}</NumberText>
              </IconLeftContainer>
            </DetailHeader>
          </DetailHeaderContainer>

          <DetailPostContainer>
            <DetailPostCategory>
              <DetailPostTitle>필더</DetailPostTitle>
              <HeaderUser>
                <Touchable>
                  {isUserProfile ? (
                    <Image
                      style={{ height: 24, width: 24, borderRadius: 50 }}
                      source={{ uri: profileImg }}
                    />
                  ) : (
                    <FontAwesome
                      name="user-circle"
                      size={24}
                      color={GlobalStyles.darkGrayColor}
                    />
                  )}
                </Touchable>
                <Touchable>
                  <Nick>{post.user.nick}</Nick>
                </Touchable>
                <Touchable>
                  <Follow>팔로우</Follow>
                </Touchable>
              </HeaderUser>
            </DetailPostCategory>

            <DetailPostCategory style={{ height: 200 }}>
              <DetailPostTitle>설명</DetailPostTitle>
              <ScrollView>
                <View flex={1} onStartShouldSetResponder={() => true}>
                  {post.content.trim().length > 0 ? (
                    <HTMLView value={post.content} style={{ padding: 3 }} />
                  ) : (
                    <DetailPostText>내용이 없습니다.</DetailPostText>
                  )}
                </View>
              </ScrollView>
            </DetailPostCategory>

            <DetailPostCategory>
              <DetailPostTitle>분야</DetailPostTitle>
              <TagContainer>
                <TagText>건축</TagText>
              </TagContainer>
            </DetailPostCategory>

            <DetailPostCategory>
              <DetailPostTitle>사용 툴</DetailPostTitle>
              <DetailPostText></DetailPostText>
            </DetailPostCategory>

            <DetailPostCategory>
              <DetailPostTitle>태그</DetailPostTitle>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <View flex={1} onStartShouldSetResponder={() => true}>
                  <TagContainer>
                    {post.hashtags.map((hashtag, index) => (
                      <TagText key={index}>{hashtag.title}</TagText>
                    ))}
                  </TagContainer>
                </View>
              </ScrollView>
            </DetailPostCategory>

            <DetailPostCategory>
              <DetailPostTitle>라이센스</DetailPostTitle>
              <DetailPostText></DetailPostText>
            </DetailPostCategory>
          </DetailPostContainer>
        </DetailContainer>
      </ModalContainer>
    </Modal>
  );
};

export default PostBottomModal;
