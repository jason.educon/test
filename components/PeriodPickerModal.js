/**
 * PeriodPickerModal
 *
 * - 프로젝트 기간 선택을 위한 모달입니다.
 * - 달력을 노출하고 하루, 혹은 기간을 선택할 수 있습니다.
 */
import React from "react";
import { Dimensions } from "react-native";

import Modal from "react-native-modal";
import CalendarPicker from "react-native-calendar-picker";
import Moment from "moment";

import styled from "styled-components";
import GlobalStyles from "../styles";
import { AntDesign } from "@expo/vector-icons";

import {
  Touchable,
  Header_FilterModal,
  Title_FilterModal,
  Desc_FilterModal,
} from "./GlobalStyledComponent";

const { width, height } = Dimensions.get("window");

const ModalContainer = styled.View`
  background-color: white;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  padding: 15px;
  border-color: rgba(0, 0, 0, 0.1);
  height: ${height * 0.9}px;
`;

const SelectedDateContainer = styled.View`
  background-color: ${GlobalStyles.lightGrayColor};
  border-radius: 15px;
  align-items: center;
  padding: 10px;
  margin: 10px 15px;
`;

const SelectedDateText = styled.Text`
  font-family: "regular";
  font-size: 12px;
  padding: 0px;
  include-font-padding: false;
  text-align-vertical: center;
`;

const PeriodPickerModal = ({
  visibleModal,
  closeModal,
  selectedStartDate,
  selectedEndDate,
  setSelectedStartDate,
  setSelectedEndDate,
}) => {
  const minDate = new Date(); // Today
  const maxDate = new Date(2017, 6, 3);
  const startDate = selectedStartDate
    ? Moment(selectedStartDate).format("YYYY.MM.DD").toString()
    : "";
  const endDate = selectedEndDate
    ? Moment(selectedEndDate).format("YYYY.MM.DD").toString()
    : "";

  const onDateChange = (date, type) => {
    if (type === "END_DATE") {
      setSelectedEndDate(date);
    } else {
      setSelectedStartDate(date);
      setSelectedEndDate(null);
    }
  };

  return (
    <Modal
      propagateSwipe={true}
      isVisible={visibleModal}
      onSwipeComplete={closeModal}
      onBackdropPress={closeModal}
      swipeDirection={["down"]}
      style={{ justifyContent: "flex-end", margin: 0 }}
    >
      <ModalContainer>
        <Touchable onPress={closeModal} style={{ alignSelf: "center" }}>
          <AntDesign
            name="closecircle"
            size={24}
            color={GlobalStyles.feeeldColor}
          />
        </Touchable>
        <Header_FilterModal>
          <Title_FilterModal>프로젝트 기간</Title_FilterModal>
          <Desc_FilterModal>
            프로젝트의 기간을 설정해서 시간의 발자국을 남겨 볼까요?
          </Desc_FilterModal>
        </Header_FilterModal>
        <SelectedDateContainer>
          {selectedStartDate == null && selectedEndDate == null ? (
            <SelectedDateText>기간을 선택해 주세요</SelectedDateText>
          ) : (
            <SelectedDateText>
              {startDate} ~ {endDate}
            </SelectedDateText>
          )}
        </SelectedDateContainer>
        <CalendarPicker
          allowRangeSelection={true}
          startFromMonday={true}
          todayBackgroundColor={GlobalStyles.lightGrayColor}
          selectedDayColor={GlobalStyles.feeeldColor}
          selectedDayTextColor="black"
          onDateChange={onDateChange}
          weekdays={["월", "화", "수", "목", "금", "토", "일"]}
          months={[
            "1월",
            "2월",
            "3월",
            "4월",
            "5월",
            "6월",
            "7월",
            "8월",
            "9월",
            "10월",
            "11월",
            "12월",
          ]}
          previousTitle="<"
          nextTitle=">"
        />
      </ModalContainer>
    </Modal>
  );
};

export default PeriodPickerModal;
