import React from "react";
import PropTypes from "prop-types";
import { ActivityIndicator } from "react-native";

import constants from "../constants";

import styled from "styled-components";
import styles from "../styles";

const Touchable = styled.TouchableOpacity``;

const Container = styled.View`
  background-color: ${(props) =>
    props.bgColor ? props.bgColor : props.theme.feeeldColor};
  padding: 15px;
  margin-top: 20px;
  border-radius: 50px;
  width: ${constants.width / 1.1}px;
`;

const Text = styled.Text`
  color: #fff;
  font-size: 16px;
  text-align: center;
  font-weight: 600;
  font-family: "medium";
`;

const AuthButton = ({ text, onPress, loading = false, bgColor = null }) => (
  <Touchable disabled={loading} onPress={onPress}>
    <Container bgColor={bgColor}>
      {loading ? <ActivityIndicator color={"white"} /> : <Text>{text}</Text>}
    </Container>
  </Touchable>
);

AuthButton.propTypes = {
  loading: PropTypes.bool,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default AuthButton;
