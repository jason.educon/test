/**
 * 작품 상세 정보 설정 스크린에서의 툴 필터 모달
 *
 * [!] 태그를 우로 슬라이딩 한 상태로 태그 입력시, 스크롤 초기화를 위해서 useRef를 사용하였다.
 */
import React, { useState, useRef } from "react";

import { View, Text, KeyboardAvoidingView, ScrollView } from "react-native";

import Modal from "react-native-modal";

import { AntDesign } from "@expo/vector-icons";

import useFilterModalInput from "../hooks/useFilterModalInput";

import { Feather } from "@expo/vector-icons";

import GlobalStyles from "../styles";

import {
  Touchable,
  Title_FilterModal,
  Desc_FilterModal,
  TagList_FilterModal,
  TagReco_FilterModal,
  TagInput_FilterModal,
  TagInputIcon_FilterModal,
  TagInputContainer_FilterModal,
  TagContainer_FilterModal,
  TagText_FilterModal,
  Header_FilterModal,
  ModalContainer_FilterModal,
  SelectedTagContainer
} from "./GlobalStyledComponent";

const toolList = [
  "Adobe Photoshop",
  "Unreal Engine",
  "Rhinoceros",
  "Twinmotion",
  "Revit",
  "Adobe Lightroom",
  "Adobe Indesign",
  "Adobe Illustrator",
  "Adobe XD"
];

const ToolFilterModal = ({
  visibleModal,
  closeModal,
  selectedTools,
  setSelectedTools
}) => {
  const sliderRef = useRef();
  const initScroll = () => {
    sliderRef.current.scrollTo({
      x: 0,
      y: 0,
      animated: true
    });
  };
  const [recoTools, setRecoTools] = useState([]);

  const filter = tool => {
    const filteredList = [];
    const lowerCaseTool = tool.toLowerCase().trim();
    for (let i = 0; i < toolList.length; i++) {
      if (
        toolList[i]
          .toLowerCase()
          .trim()
          .includes(lowerCaseTool)
      ) {
        filteredList.push(toolList[i]);
      }
    }
    setRecoTools(filteredList);
  };

  const tagInput = useFilterModalInput("", 30, initScroll, filter);

  const addTool = newTool => {
    if (selectedTools.length >= 5) {
      alert("사용 프로그램은 5개 까지 선택할 수 있습니다.");
      return;
    }
    const clonedTools = selectedTools.slice();
    for (let i = 0; i < clonedTools.length; i++) {
      if (clonedTools[i] === newTool) {
        return;
      }
    }
    clonedTools.push(newTool);
    setSelectedTools(clonedTools);
    tagInput.setValue("");
  };

  const removeTool = tool => {
    const clonedTools = selectedTools.slice();
    for (let i = 0; i < clonedTools.length; i++) {
      if (clonedTools[i] === tool) {
        clonedTools.splice(i, 1);
        break;
      }
    }
    setSelectedTools(clonedTools);
  };

  return (
    <Modal
      propagateSwipe={true}
      isVisible={visibleModal}
      onSwipeComplete={closeModal}
      onBackdropPress={closeModal}
      swipeDirection={["down"]}
      avoidKeyboard={true}
      style={{ justifyContent: "flex-end", margin: 0 }}
    >
      <ModalContainer_FilterModal>
        <Touchable onPress={closeModal} style={{ alignSelf: "center" }}>
          <AntDesign
            name="closecircle"
            size={24}
            color={GlobalStyles.feeeldColor}
          />
        </Touchable>
        <Header_FilterModal>
          <Title_FilterModal>
            사용 프로그램 ({selectedTools.length}/5)
          </Title_FilterModal>
          <Desc_FilterModal>
            필디 회원들은 궁금합니다! 어떤 툴을 이용해 작업하셨나요?
          </Desc_FilterModal>
          <SelectedTagContainer>
            <TagList_FilterModal>
              {selectedTools.length == 0 ? (
                <Text>프로그램을 선택해주세요.</Text>
              ) : (
                selectedTools.map((tag, index) => (
                  <Touchable key={index} onPress={() => removeTool(tag)}>
                    <TagText_FilterModal>
                      {tag + " "}
                      <Feather name="x" size={12} color="red" />
                    </TagText_FilterModal>
                  </Touchable>
                ))
              )}
            </TagList_FilterModal>
          </SelectedTagContainer>
        </Header_FilterModal>
        <TagReco_FilterModal>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            ref={sliderRef}
          >
            <View flex={1} onStartShouldSetResponder={() => true}>
              <TagContainer_FilterModal>
                {recoTools.length === 0 ? (
                  <TagText_FilterModal>
                    <Text>결과 없음</Text>
                  </TagText_FilterModal>
                ) : (
                  recoTools.map((tag, index) => (
                    <Touchable key={index} onPress={() => addTool(tag)}>
                      <TagText_FilterModal>
                        <Feather name="plus" size={12} color="black" />
                        {tag}
                      </TagText_FilterModal>
                    </Touchable>
                  ))
                )}
              </TagContainer_FilterModal>
            </View>
          </ScrollView>
        </TagReco_FilterModal>

        <TagInputContainer_FilterModal>
          <TagInputIcon_FilterModal>
            <Feather name="hash" size={24} color="black" />
          </TagInputIcon_FilterModal>
          <TagInput_FilterModal
            editable
            autoFocus={true}
            maxLength={40}
            value={tagInput.value}
            onChangeText={tagInput.onChange}
          />
          <TagInputIcon_FilterModal>
            <Touchable onPress={() => tagInput.setValue("")}>
              <Feather name="x" size={24} color="black" />
            </Touchable>
          </TagInputIcon_FilterModal>
        </TagInputContainer_FilterModal>
      </ModalContainer_FilterModal>
    </Modal>
  );
};

export default ToolFilterModal;
