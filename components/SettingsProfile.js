/**
 * [SettingsProfile.js]
 * - '설정 > 프로필 설정' 페이지
 * - 닉네임 변경, 프로필 사진 변경
 */
import React, { useState } from "react";
import { View, Text, Dimensions } from "react-native";

import styled from "styled-components";
import GlobalStyles from "../styles";
import { Entypo } from "@expo/vector-icons";

import { useIsLoggedIn, useLogOut } from "../AuthContext";

import { Touchable } from "./GlobalStyledComponent";

import constants from "../constants";
import { setNavHeader } from "../utils/navHeader";
import FloatingButton from "./FloatingButton";

const Border = styled.View`
  border-bottom-color: ${GlobalStyles.editorBorderColor};
  border-bottom-width: 2px;
  margin: 15px;
`;

const { width, height } = Dimensions.get("window");

const SettingsProfileView = styled.SafeAreaView`
  flex: 1;
  background-color: white;
  padding-top: ${constants.statusBarHeight}px;
`;

const SettingProfileContainer = styled.View`
  flex: 1;
  margin: 20px;
`;

const SettingText = styled.Text`
  ${GlobalStyles.h1};
  padding-bottom: 10px;
  border-bottom-width: 3px;
  border-color: ${GlobalStyles.darkGrayColor};
  
`;

const SettingContents = styled.View`
  flex: 2;
  flex-direction: column;
  margin: 0 5px;
  justify-content: space-between;
`;

const SettingItem = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 10px 0;
`;

const ItemText = styled.Text`
  ${GlobalStyles.h2};
  text-align: left;
  flex: 1;
`;

const ItemChevron = styled.TouchableOpacity`
  flex: 1;
`;

const NickNameSetting = styled.View`
  flex: 2;
  padding: 15px 5px;
`;

const NickNameInput = styled.TextInput`
  flex: 2;
  height: 30px;
  background-color: red;
`;

export default ({ navigation }) => {
  setNavHeader({ navigation, visible: false });

  return (
    <SettingsProfileView>
      <SettingProfileContainer>
      <SettingText>프로필 설정</SettingText>
        <Border></Border>
        <SettingContents>
          <NickNameSetting>
            <SettingItem>
              <ItemText>닉네임</ItemText>
              <NickNameInput>{" "}</NickNameInput>
            </SettingItem>
          </NickNameSetting>
          </SettingContents>
        </SettingProfileContainer>
        <FloatingButton
            icon="left"
            onPress={() => navigation.goBack()}
            direction="left"
            visible={true}
          />
    </SettingsProfileView>
  );
};
