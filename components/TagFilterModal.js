/**
 * 작품 상세 정보 설정 스크린에서의 태그 필터 모달
 *
 * [!] 태그를 우로 슬라이딩 한 상태로 태그 입력시, 스크롤 초기화를 위해서 useRef를 사용하였다.
 */
import React, { useState, useRef } from "react";

import { View, Text, KeyboardAvoidingView, ScrollView } from "react-native";

import Modal from "react-native-modal";

import { AntDesign } from "@expo/vector-icons";

import useFilterModalInput from "../hooks/useFilterModalInput";

import { Feather } from "@expo/vector-icons";

import GlobalStyles from "../styles";

import {
  Touchable,
  Title_FilterModal,
  Desc_FilterModal,
  TagList_FilterModal,
  TagReco_FilterModal,
  TagInput_FilterModal,
  TagInputIcon_FilterModal,
  TagInputContainer_FilterModal,
  TagContainer_FilterModal,
  TagText_FilterModal,
  Header_FilterModal,
  ModalContainer_FilterModal,
  SelectedTagContainer
} from "./GlobalStyledComponent";

const shortenText = s => {
  if (s.length > 10) {
    return s.slice(0, 7) + "...";
  }
  return s;
};

const TagFilterModal = ({
  visibleModal,
  closeModal,
  selectedTags,
  setSelectedTags
}) => {
  const sliderRef = useRef();
  const initScroll = () => {
    sliderRef.current.scrollTo({
      x: 0,
      y: 0,
      animated: true
    });
  };
  const tagInput = useFilterModalInput("", 20, initScroll);
  const [recoTags, setRecoTags] = useState([
    "추천태그1",
    "추천태그2",
    "추천태그3",
    "추천태그4",
    "추태천그5"
  ]);

  const addTag = (newTag, self) => {
    if (selectedTags.length >= 20) {
      alert("태그는 20개 까지 추가할 수 있습니다.");
      return;
    }
    const clonedTags = selectedTags.slice();
    for (let i = 0; i < clonedTags.length; i++) {
      if (clonedTags[i] === newTag) {
        return;
      }
    }
    clonedTags.push(newTag);
    setSelectedTags(clonedTags);
    if (self) {
      tagInput.setValue("");
    }
  };

  const removeTag = tag => {
    const clonedTags = selectedTags.slice();
    for (let i = 0; i < clonedTags.length; i++) {
      if (clonedTags[i] === tag) {
        clonedTags.splice(i, 1);
        break;
      }
    }
    setSelectedTags(clonedTags);
  };

  return (
    <Modal
      propagateSwipe={true}
      isVisible={visibleModal}
      onSwipeComplete={closeModal}
      onBackdropPress={closeModal}
      swipeDirection={[]}
      avoidKeyboard={true}
      style={{ justifyContent: "flex-end", margin: 0 }}
    >
      <ModalContainer_FilterModal>
        <Touchable onPress={closeModal} style={{ alignSelf: "center" }}>
          <AntDesign
            name="closecircle"
            size={24}
            color={GlobalStyles.feeeldColor}
          />
        </Touchable>
        <Header_FilterModal>
          <Title_FilterModal>
            프로젝트 태그 ({selectedTags.length}/20)
          </Title_FilterModal>
          <Desc_FilterModal>
            세상에 더욱 잘 퍼지도록, 작품의 키워드를 입력해 주세요.
          </Desc_FilterModal>
          <SelectedTagContainer>
            <TagList_FilterModal>
              {selectedTags.length == 0 ? (
                <Text>태그를 작성해주세요.</Text>
              ) : (
                selectedTags.map((tag, index) => (
                  <Touchable key={index} onPress={() => removeTag(tag)}>
                    <TagText_FilterModal>
                      {shortenText(tag) + " "}
                      <Feather name="x" size={12} color="red" />
                    </TagText_FilterModal>
                  </Touchable>
                ))
              )}
            </TagList_FilterModal>
          </SelectedTagContainer>
        </Header_FilterModal>
        <TagReco_FilterModal>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            ref={sliderRef}
          >
            <View flex={1} onStartShouldSetResponder={() => true}>
              <TagContainer_FilterModal>
                {tagInput.value.length ? (
                  <Touchable onPress={() => addTag(tagInput.value, true)}>
                    <TagText_FilterModal>
                      <Feather name="plus" size={12} color="black" />
                      {tagInput.value}
                    </TagText_FilterModal>
                  </Touchable>
                ) : (
                  <Text></Text>
                )}
                {recoTags.map((tag, index) => (
                  <Touchable key={index} onPress={() => addTag(tag)}>
                    <TagText_FilterModal>
                      <Feather name="plus" size={12} color="black" />
                      {tag}
                    </TagText_FilterModal>
                  </Touchable>
                ))}
              </TagContainer_FilterModal>
            </View>
          </ScrollView>
        </TagReco_FilterModal>
        <TagInputContainer_FilterModal>
          <TagInputIcon_FilterModal>
            <Feather name="hash" size={24} color="black" />
          </TagInputIcon_FilterModal>
          <TagInput_FilterModal
            editable
            autoFocus={true}
            maxLength={40}
            value={tagInput.value}
            onChangeText={tagInput.onChange}
          />
          <TagInputIcon_FilterModal>
            <Touchable onPress={() => tagInput.setValue("")}>
              <Feather name="x" size={24} color="black" />
            </Touchable>
          </TagInputIcon_FilterModal>
        </TagInputContainer_FilterModal>
      </ModalContainer_FilterModal>
    </Modal>
  );
};

export default TagFilterModal;
