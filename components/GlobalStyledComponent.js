/**
 * 재사용되는 스타일드 컴포넌트를 작성하는 곳입니다.
 * 두 곳 이상에서 사용되고, 변경이 어느정도 빈번할 것으로 예상되는 경우 이곳에 추가하여 활용합니다.
 *
 * suffix _분류명의 규칙을 따라주세요.
 */
import styled from "styled-components";
import GlobalStyles from "../styles";

import { Dimensions } from "react-native";

export const FloatingButtonContainer = styled.View`
  flex: 1;
  background-color: white;
`;

export const Touchable = styled.TouchableOpacity``;

/**
 * Style in (Tag/Tool)FilterModal
 * suffix _FM
 *
 * [!] flex-wrap을 통해 컨텐츠가 넘칠 경우 아래로 자동으로 줄바뀜 처리를 할 수 있다.
 */

const { width, height } = Dimensions.get("window");

export const ModalContainer_FilterModal = styled.View`
  background-color: white;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  padding-top: 15px;
  border-color: rgba(0, 0, 0, 0.1);
  height: ${Platform.OS == "ios" ? height * 0.5 : height * 0.5}px;
`;

export const Header_FilterModal = styled.View`
  margin-top: 10px;
`;

export const Title_FilterModal = styled.Text`
  font-family: "medium";
  font-size: 20px;
  margin-bottom: 5px;
  padding: 0 15px;
  include-font-padding: false;
  text-align-vertical: center;
`;

export const Desc_FilterModal = styled.Text`
  font-family: "regular";
  font-size: 14px;
  color: ${GlobalStyles.darkGrayColor};
  padding: 0 15px;
`;

export const SelectedTagContainer = styled.ScrollView`
  height: 100px;
  margin: 5px 15px;
  margin-top: 10px;
  background-color: ${GlobalStyles.lightGrayColor};
  border-radius: 10px;
  padding: 10px;
`;

export const TagList_FilterModal = styled.View`
  padding-bottom: 20px;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const TagContainer_FilterModal = styled.View`
  flex-direction: row;
`;

export const TagText_FilterModal = styled.Text`
  ${GlobalStyles.feeeldBox};
  font-size: 14px;
  color: ${GlobalStyles.darkGrayColor};
  padding: 0 8px;
  text-align: center;
  margin: 3px;
  font-family: "regular";
`;

export const TagReco_FilterModal = styled.View`
  margin-top: 15px;
  min-height: 55px;
  border: 1px solid rgb(239, 239, 239);
  padding: 15px;
  flex-direction: row;
`;

export const TagInputContainer_FilterModal = styled.View`
  margin: 0 15px;
  margin-top: 10px;
  flex-direction: row;
  align-items: center;
`;

export const TagInputIcon_FilterModal = styled.View`
  flex: 1;
  margin-left: 5px;
`;

export const TagInput_FilterModal = styled.TextInput`
  flex: 8;
  font-size: 15px;
  font-family: "regular";
`;
