/**
 * 저작권 설정의 커먼 크리에이티브 라이센스 상세히보기 클릭시 보이는 바텀 모달을 구현
 */
import React, { useState } from "react";

import {
  View,
  Text,
  Image,
  Button,
  ScrollView,
  Platform,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native";

import { FontAwesome5 } from "@expo/vector-icons";
import HTMLView from "react-native-htmlview";
import Modal from "react-native-modal";

import { FontAwesome, Entypo, AntDesign } from "@expo/vector-icons";
import styled from "styled-components";
import styles from "../styles";
import GlobalStyles from "../styles";

const { width, height } = Dimensions.get("window");

const Touchable = styled.TouchableOpacity``;

const ModalContainer = styled.View`
  background-color: white;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  padding: 15px;
  border-color: rgba(0, 0, 0, 0.1);
  height: ${height * 0.9}px;
`;

const DetailContainer = styled.View`
  margin-top: 10px;
`;

const CopyrightModalTitle = styled.Text`
  margin-top: 10px;
  font-size: 20px;
  text-align: center;
`;

const DetailTitle = styled.Text`
  font-size: 15px;
  text-align: left;
`;

const Border = styled.View`
  border-bottom-color: ${GlobalStyles.editorBorderColor};
  border-bottom-width: 1px;
  margin: 20px;
`;

const IconContainer = styled.View`
  flex-direction: row;
`;

const DetailSubTitle = styled.Text``;

const DetailDescription = styled.Text``;

const CopyrightModal = ({ visibleModal, closeModal, post }) => {
  return (
    <Modal
      propagateSwipe={true}
      isVisible={visibleModal}
      onSwipeComplete={closeModal}
      onBackdropPress={closeModal}
      swipeDirection={["left", "right", "down"]}
      style={{ justifyContent: "flex-end", margin: 0 }}
    >
      <ModalContainer>
        <Touchable onPress={closeModal} style={{ alignSelf: "center" }}>
          <AntDesign name="closecircle" size={24} color={styles.feeeldColor} />
        </Touchable>
        <CopyrightModalTitle
          style={{
            fontFamily: "NotoSansKR_700Bold",
          }}
        >
          {"저작권 설명"}
        </CopyrightModalTitle>

        <ScrollView>
          <View flex={1} onStartShouldSetResponder={() => true}>
            <DetailContainer>
              {/* 모두 선택 해제시 */}

              <IconContainer>
                <FontAwesome5 name="tired" size={30} color={"black"} />
              </IconContainer>
              <DetailTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"All Rights Reserved"}
              </DetailTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "명시적인 허가 없이 다른 사용자들이 내 작품을 사용할 수 없습니다"
                }
              </DetailDescription>
              <Border></Border>

              {/* 저작자 표시 */}

              <IconContainer>
                <FontAwesome5
                  name="creative-commons-by"
                  size={30}
                  color={"black"}
                />
              </IconContainer>
              <DetailTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"저작자 표시(CC BY)"}
              </DetailTitle>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"가능한 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 복사 및 배포를 할 수 있습니다.(반드시 저작자 및 출처를 표시합니다.\n- 상업적 이용이 가능합니다.\n- 이 저작물을 변경하거나, 이용해 2차 저작물을 만들어도 됩니다.(반드시 원저작자 및 출처를 표시합니다.)\n- 2차 저작물의 라이선스를 자유롭게 선택해도 됩니다."
                }
              </DetailDescription>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"제한적 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 저작자 및 출처만 표시한다면, 제한없이 자유롭게 이용할 수 있습니다."
                }
              </DetailDescription>
              <Border></Border>

              {/* 저작자표시-비영리 */}

              <IconContainer>
                <FontAwesome5
                  name="creative-commons-by"
                  size={30}
                  color={"black"}
                />
                <FontAwesome5
                  name="creative-commons-nc"
                  size={30}
                  color={"black"}
                />
              </IconContainer>

              <DetailTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"저작자표시-비영리(CC BY-NC)"}
              </DetailTitle>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"가능한 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 복사 및 배포를 할 수 있습니다.(반드시 저작자 및 출처를 표시합니다.\n- 이 저작물을 변경하거나, 이용해 2차 저작물을 만들어도 됩니다.(반드시 원저작자 및 출처를 표시합니다.)\n- 2차 저작물의 라이선스를 자유롭게 선택해도 됩니다."
                }
              </DetailDescription>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"제한적 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {"- 상업적 이용이 불가능합니다."}
              </DetailDescription>
              <Border></Border>

              {/* 저작자표시-변경금지 */}

              <IconContainer>
                <FontAwesome5
                  name="creative-commons-by"
                  size={30}
                  color={"black"}
                />
                <FontAwesome5
                  name="creative-commons-nd"
                  size={30}
                  color={"black"}
                />
              </IconContainer>

              <DetailTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"저작자표시-변경금지(CC BY-ND)"}
              </DetailTitle>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"가능한 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 복사 및 배포를 할 수 있습니다.(반드시 저작자 및 출처를 표시합니다.)\n- 상업적 이용이 가능합니다."
                }
              </DetailDescription>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"제한적 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 저작물을 변경하거나, 이용해 2차 저작물을 만들어서는 안됩니다."
                }
              </DetailDescription>
              <Border></Border>

              {/* 저작자표시-동일조건변경허락 */}

              <IconContainer>
                <FontAwesome5
                  name="creative-commons-by"
                  size={30}
                  color={"black"}
                />
                <FontAwesome5
                  name="creative-commons-sa"
                  size={30}
                  color={"black"}
                />
              </IconContainer>

              <DetailTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"저작자표시-동일조건변경허락(CC BY-SA)"}
              </DetailTitle>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"가능한 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 복사 및 배포를 할 수 있습니다.(반드시 저작자 및 출처를 표시합니다.)\n- 상업적 이용이 가능합니다.\n- 이 저작물을 변경하거나, 이용해 2차 저작물을 만들어도 됩니다.(반드시 원저작자 및 출처를 표시합니다.)"
                }
              </DetailDescription>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"제한적 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {"- 2차 저작물에 원저작물과 동일한 라이선스를 적용해야 합니다."}
              </DetailDescription>
              <Border></Border>

              {/* 저작자표시-비영리-동일조건 변경허락 */}

              <IconContainer>
                <FontAwesome5
                  name="creative-commons-by"
                  size={30}
                  color={"black"}
                />
                <FontAwesome5
                  name="creative-commons-nc"
                  size={30}
                  color={"black"}
                />
                <FontAwesome5
                  name="creative-commons-sa"
                  size={30}
                  color={"black"}
                />
              </IconContainer>

              <DetailTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"저작자표시-비영리-동일조건 변경허락 (BY-NC-SA)"}
              </DetailTitle>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"가능한 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 복사 및 배포를 할 수 있습니다.(반드시 저작자 및 출처를 표시합니다.)\n- 상업적 이용이 가능합니다."
                }
              </DetailDescription>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"제한적 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 저작물을 변경하거나, 이용해 2차 저작물을 만들어서는 안됩니다."
                }
              </DetailDescription>
              <Border></Border>

              {/* 저작자표시-비영리-변경금지 */}

              <IconContainer>
                <FontAwesome5
                  name="creative-commons-by"
                  size={30}
                  color={"black"}
                />
                <FontAwesome5
                  name="creative-commons-nc"
                  size={30}
                  color={"black"}
                />
                <FontAwesome5
                  name="creative-commons-nd"
                  size={30}
                  color={"black"}
                />
              </IconContainer>

              <DetailTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"저작자표시-비영리-변경금지 (BY-NC-ND)"}
              </DetailTitle>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"가능한 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 복사 및 배포를 할 수 있습니다.(반드시 저작자 및 출처를 표시합니다.)"
                }
              </DetailDescription>
              <DetailSubTitle
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                }}
              >
                {"제한적 이용"}
              </DetailSubTitle>
              <DetailDescription
                style={{
                  fontFamily: "NotoSansKR_400Regular",
                }}
              >
                {
                  "- 상업적 이용이 불가능합니다.\n- 이 저작물을 변경하거나, 이용해 2차 저작물을 만들어서는 안됩니다."
                }
              </DetailDescription>
              <Border></Border>
            </DetailContainer>
          </View>
        </ScrollView>
      </ModalContainer>
    </Modal>
  );
};

export default CopyrightModal;
