/**
 * [Settings.js]
 * - 앱 및 계정 등 환경설정 화면
 * - 로그인과 비로그인 상태에 따라 다르게 노출
 * - 로그인 시: 계정 설정, 프로필 설정, 오류 신고, 약관 노출
 * - 비로그인 시: 오류 신고, 약관 노출
 */
import React, { useState } from "react";
import { View, Text, Alert } from "react-native";

import styled from "styled-components";
import GlobalStyles from "../styles";
import { Entypo } from "@expo/vector-icons";

import { useIsLoggedIn, useLogOut } from "../AuthContext";

import { Touchable } from "./GlobalStyledComponent";

const Border = styled.View`
  border-bottom-color: ${GlobalStyles.editorBorderColor};
  border-bottom-width: 2px;
  margin: 15px;
`;

const SettingView = styled.View`
  flex: 1;
  margin: 20px;
`;

const SettingText = styled.Text`
  ${GlobalStyles.h1};
  padding-bottom: 10px;
  border-bottom-width: 3px;
  border-color: ${GlobalStyles.darkGrayColor};
`;

const SettingContents = styled.View`
  flex: 2;
  flex-direction: column;
  margin: 0 5px;
  justify-content: space-between;
`;

const SettingItem = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 10px 0;
`;

const ItemText = styled.Text`
  ${GlobalStyles.h2};
  text-align: left;
  flex: 9;
`;

const ItemChevron = styled.TouchableOpacity`
  flex: 1;
`;

const AccountsSettings = styled.View`
  flex: 2;
  padding: 15px 5px;
`;

const ServiceSettings = styled.View`
  flex: 1;
  align-items: flex-end;
  padding: 15px 5px;
`;

const VersionInfo = styled.View`
  flex: 1;
  align-items: center;
  padding: 15px 5px;
`;

const VersionInfoText = styled.Text`
  ${GlobalStyles.h4};
  color: ${GlobalStyles.darkGrayColor};
`;

const VersionInfoSmallText = styled.Text`
  ${GlobalStyles.h5};
  color: ${GlobalStyles.grayColor};
`;

const VersionImage = styled.Image`
  width: 50px;
  height: 50px;
`;

const Logout = styled.Text`
  ${GlobalStyles.h2};
  align-items: center;
`;

export default ({ navigation }) => {
  const isLoggedIn = useIsLoggedIn();
  const logout = useLogOut();

  return (
    <SettingView>
      <SettingText>설정</SettingText>
      <Border></Border>
      <SettingContents>
        {/* 로그인 시 */}
        {isLoggedIn ? (
          <AccountsSettings>
            <SettingItem>
              <ItemText>계정 설정</ItemText>
              <ItemChevron
                onPress={() => {
                  navigation.navigate("SettingsAccount");
                }}
              >
                <Entypo name="chevron-right" size={30} color="black" />
              </ItemChevron>
            </SettingItem>
            <SettingItem>
              <ItemText>프로필 설정</ItemText>
              <ItemChevron
                onPress={() => {
                  navigation.navigate("SettingsProfile");
                }}
              >
                <Entypo name="chevron-right" size={30} color="black" />
              </ItemChevron>
            </SettingItem>
          </AccountsSettings>
        ) : (
          <></>
        )}
        {/* 로그인 & 비로그인 시 */}
        <ServiceSettings>
          <SettingItem>
            <ItemText>오류 신고 및 문제 해결</ItemText>
            <ItemChevron
              onPress={() => {
                navigation.navigate("SettingsIssues");
              }}
            >
              <Entypo name="chevron-right" size={30} color="black" />
            </ItemChevron>
          </SettingItem>
          <SettingItem>
            <ItemText>서비스 약관 및 개인정보 보호정책</ItemText>
            <ItemChevron
              onPress={() => {
                navigation.navigate("SettingsTerms");
              }}
            >
              <Entypo name="chevron-right" size={30} color="black" />
            </ItemChevron>
          </SettingItem>
        </ServiceSettings>
      </SettingContents>
      <Border></Border>
      <VersionInfo>
        <VersionImage source={require("../assets/feeeld_logo.png")} />
        <VersionInfoText>베타 버전을 사용 중입니다.</VersionInfoText>
        <VersionInfoSmallText>현재 버전: 3.0.0 (beta)</VersionInfoSmallText>
        <Text>{" "}</Text>
        <Touchable onPress={logout}>
          <Logout>로그아웃</Logout>
        </Touchable>
      </VersionInfo>
      
    </SettingView>
  );
};
