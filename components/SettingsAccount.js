/**
 * [SettingAccount.js]
 * - '설정 > 계정 설정' 페이지
 * - 이메일 확인, 비밀번호 변경, 계정 탈퇴 기능
 */
import React, { useState } from "react";
import { View, Text } from "react-native";

import styled from "styled-components";
import GlobalStyle from "../styles";
import { Entypo } from "@expo/vector-icons";

import { useIsLoggedIn, useLogOut } from "../AuthContext";

import { Touchable } from "./GlobalStyledComponent";
import { setNavHeaderInSettings } from "../utils/navHeader";

export default ({ navigation }) => {
  setNavHeaderInSettings({ navigation, title: `계정 설정` });
  return (
    <View>
      <Text>계정 설정 페이지</Text>
    </View>
  );
};
