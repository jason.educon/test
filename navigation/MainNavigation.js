/**
 * NavController.js 컴포넌트에 포함되는 메인 네비게이션이다.
 *
 * todo ; screen 뒤에는 ~Screen suffix를 붙이는게 관례라고 함.
 */
import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../screens/Tabs/Home";
import Viewer from "../screens/Tabs/Viewer";
import Editor from "../screens/Tabs/Editor";
import Search from "../screens/Tabs/Search";
import Upload from "../screens/Tabs/Upload";
import Message from "../screens/Tabs/Message";
import User from "../screens/Tabs/User";
import PhotoPicker from "../screens/Tabs/PhotoPicker";
import PhotoEditor from "../screens/Tabs/PhotoEditor";

import AuthHome from "../screens/Auth/AuthHome";
import PhoneAuth from "../screens/Auth/PhoneAuth";
import CodeAuth from "../screens/Auth/CodeAuth";
import JoinBasic from "../screens/Auth/JoinBasic";
import LocalLogin from "../screens/Auth/LocalLogin";
import ConfirmPolicy from "../screens/Auth/ConfirmPolicy";
import MetaEditor from "../screens/Tabs/MetaEditor";

import SettingsAccount from "../components/SettingsAccount";
import SettingsProfile from "../components/SettingsProfile";
import SettingsIssues from "../components/SettingsIssues";
import SettingsTerms from "../components/SettingsTerms";
import Terms1 from "../components/Terms1";
import Terms2 from "../components/Terms2";

const Stack = createStackNavigator();

export default function MainNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerTitle: "",
        }}
      >
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Viewer" component={Viewer} />
        <Stack.Screen name="Editor" component={Editor} />
        <Stack.Screen name="PhotoPicker" component={PhotoPicker} />
        <Stack.Screen name="PhotoEditor" component={PhotoEditor} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="Upload" component={Upload} />
        <Stack.Screen name="User" component={User} />
        <Stack.Screen name="AuthHome" component={AuthHome} />
        <Stack.Screen name="LocalLogin" component={LocalLogin} />
        <Stack.Screen name="PhoneAuth" component={PhoneAuth} />
        <Stack.Screen name="CodeAuth" component={CodeAuth} />
        <Stack.Screen name="JoinBasic" component={JoinBasic} />
        <Stack.Screen name="ConfirmPolicy" component={ConfirmPolicy} />
        <Stack.Screen name="MetaEditor" component={MetaEditor} />

        <Stack.Screen name="SettingsAccount" component={SettingsAccount} />
        <Stack.Screen name="SettingsProfile" component={SettingsProfile} />
        <Stack.Screen name="SettingsIssues" component={SettingsIssues} />
        <Stack.Screen name="SettingsTerms" component={SettingsTerms} />
        <Stack.Screen name="Terms1" component={Terms1} />
        <Stack.Screen name="Terms2" component={Terms2} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
