import React, { useState, useEffect } from "react";
import { AsyncStorage } from "react-native";

import { Ionicons } from "@expo/vector-icons";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import { Asset } from "expo-asset";
import {
  useFonts,
  NotoSansKR_100Thin,
  NotoSansKR_300Light,
  NotoSansKR_400Regular,
  NotoSansKR_500Medium,
  NotoSansKR_700Bold,
  NotoSansKR_900Black,
} from "@expo-google-fonts/noto-sans-kr";

import { ThemeProvider } from "styled-components";
import styles from "./styles";

import { ApolloProvider } from "react-apollo-hooks";
import ApolloClient from "apollo-boost";
import apolloClientOptions from "./apollo";
import { InMemoryCache } from "apollo-cache-inmemory";
import { persistCache } from "apollo-cache-persist";

import NavController from "./components/NavController";
import { AuthProvider } from "./AuthContext";

import AnimatedSplash from "react-native-animated-splash-screen";

export default function App() {
  const [loaded, setLoaded] = useState(false);
  const [client, setClient] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(null); // null: check 안함, true: check 했고 로그인, false: check 했고 로그아웃
  let [fontsLoaded] = useFonts({
    regular: require("./assets/fonts/NotoSansCJKkr-Regular.otf"),
    medium: require("./assets/fonts/NotoSansCJKkr-Medium.otf"),
    bold: require("./assets/fonts/NotoSansCJKkr-Bold.otf"),
  });

  const preLoad = async () => {
    // App 시작 전 필요한 것들 preload (icon, logo)
    try {
      await Asset.loadAsync([
        require("./assets/feeeld_logo.png"),
        require("./assets/brush.png"),
        require("./assets/logo.png"),
      ]);

      const cache = new InMemoryCache();
      await persistCache({
        cache,
        storage: AsyncStorage,
      });
      const client = new ApolloClient({
        cache,
        request: async (operation) => {
          const token = await AsyncStorage.getItem("jwt");
          return operation.setContext({
            headers: { Authorization: `Bearer ${token}` },
          });
        },
        ...apolloClientOptions,
      });
      const isLoggedIn = await AsyncStorage.getItem("isLoggedIn");
      if (!isLoggedIn || isLoggedIn === "false") {
        setIsLoggedIn(false);
      } else {
        setIsLoggedIn(true);
      }
      setLoaded(true);
      setClient(client);
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    preLoad();
  }, []);

  return fontsLoaded && loaded && client && isLoggedIn !== null ? (
    <ApolloProvider client={client}>
      <ThemeProvider theme={styles}>
        <AuthProvider isLoggedIn={isLoggedIn}>
          <NavController />
        </AuthProvider>
      </ThemeProvider>
    </ApolloProvider>
  ) : (
    <AppLoading />
  );
}
