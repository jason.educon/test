/**
 * 네비게이션 헤더를 필디 앱에 맞게 설정하는 함수.
 * 스크린 마다 달라지는 헤더를 컨트롤 하는 함수로, 스크린의 뷰가 초기화 될 때 호출함으로써 설정한다.
 *
 * 헤더를 없애고 싶을 때는 visible을 false로 세팅하여 전달한다.
 *
 * [!] PhotoPicker의 헤더를 구현하기 위해 setNavHeaderInPhotoPicker를 별도로 작성하였다.
 */
import React, { Component } from "react";
import { View, Image, Text, TouchableOpacity } from "react-native";

import Toast from "react-native-tiny-toast";

import { AntDesign } from "@expo/vector-icons";

import styled from "styled-components";
import GlobalStyles from "../styles";

import { Touchable } from "../components/GlobalStyledComponent";
import constants from "../constants";

const HeaderCenterView = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const HeaderCenterText = styled.Text`
  ${GlobalStyles.h2};
  text-align: center;
`;

const HeaderLeftText = styled.Text`
  ${GlobalStyles.h1};
  text-align: left;
`;

const HeaderSideView = styled.View`
  padding-left: 15px;
  padding-right: 15px;
  flex-direction: row;
  align-items: center;
`;

const HeaderSideText = styled.Text`
  ${GlobalStyles.h3};
  text-align: center;
`;

export const setNavHeader = ({ navigation, visible }) => {
  if (visible === false) {
    navigation.setOptions({
      headerShown: false,
    });
  }
  navigation.setOptions({
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate("Home")}>
        <Image
          source={require("../assets/logo.png")}
          style={{ width: 150, height: 75, marginLeft: -20 }}
        />
      </TouchableOpacity>
    ),
    headerRight: () => (
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity onPress={() => navigation.navigate("Search")}>
          <AntDesign
            name="search1"
            size={25}
            color="black"
            style={{ margin: 10 }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("User")}>
          <AntDesign
            name="user"
            size={25}
            color="black"
            style={{ margin: 10 }}
          />
        </TouchableOpacity>
      </View>
    ),
  });
};

export const setNavHeaderInPhotoPicker = ({
  navigation,
  onCancel,
  onSubmit,
}) => {
  navigation.setOptions({
    headerTitle: () => (
      <HeaderCenterView>
        <HeaderCenterText>이미지 선택</HeaderCenterText>
      </HeaderCenterView>
    ),
    headerLeft: () => (
      <TouchableOpacity onPress={() => onCancel()}>
        <HeaderSideView>
          <AntDesign name="left" size={18} color="black" />
          <HeaderSideText>취소</HeaderSideText>
        </HeaderSideView>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity onPress={() => onSubmit()}>
        <HeaderSideView>
          <HeaderSideText>다음</HeaderSideText>
          <AntDesign name="right" size={18} color="black" />
        </HeaderSideView>
      </TouchableOpacity>
    ),
  });
};

export const setNavHeaderInPhotoEditor = ({
  navigation,
  onNext,
  onPrevious,
}) => {
  navigation.setOptions({
    headerTitle: () => (
      <HeaderCenterView>
        <HeaderCenterText>이미지 편집</HeaderCenterText>
        <Touchable
          onPress={() =>
            Toast.show(
              "이미지를 한번 터치하여 캡션을 입력할 수 있습니다.\n그리고 길게 터치하여 순서변경을 해보세요",
              {
                position: 0,
                containerStyle: {
                  width: constants.width - 30,
                  height: 130,
                  backgroundColor: "rgba(119,235,174,0.9)",
                  borderRadius: 10,
                  padding: 15,
                },
                imgSource: require("../assets/warning.png"),
                imgStyle: { width: 30, height: 30, marginBottom: 25 },
                textStyle: {
                  fontSize: 13,
                  color: "white",
                  fontFamily: "bold",
                },
                duration: 3000,
              }
            )
          }
        >
          <AntDesign
            name="questioncircleo"
            size={15}
            color={GlobalStyles.feeeldColor}
          />
        </Touchable>
      </HeaderCenterView>
    ),
    headerLeft: () => (
      <TouchableOpacity onPress={() => onPrevious()}>
        <HeaderSideView>
          <AntDesign name="left" size={18} color="black" />
          <HeaderSideText>이전</HeaderSideText>
        </HeaderSideView>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity onPress={() => onNext()}>
        <HeaderSideView>
          <HeaderSideText>다음</HeaderSideText>
          <AntDesign name="right" size={18} color="black" />
        </HeaderSideView>
      </TouchableOpacity>
    ),
  });
};

export const setNavHeaderInEditor = ({ navigation, onPrevious, onSubmit }) => {
  navigation.setOptions({
    headerTitle: () => (
      <HeaderCenterView>
        <HeaderCenterText>내용 작성</HeaderCenterText>
      </HeaderCenterView>
    ),
    headerLeft: () => (
      <TouchableOpacity onPress={() => onPrevious()}>
        <HeaderSideView>
          <AntDesign name="left" size={18} color="black" />
          <HeaderSideText>이전</HeaderSideText>
        </HeaderSideView>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity onPress={() => onSubmit()}>
        <HeaderSideView>
          <HeaderSideText>게시</HeaderSideText>
          <AntDesign name="right" size={18} color="black" />
        </HeaderSideView>
      </TouchableOpacity>
    ),
  });
};

export const setNavHeaderInSettings = ({ navigation, title }) => {
  navigation.setOptions({
    headerStyle: {
      borderBottomWidth: 3,
      borderColor: GlobalStyles.darkGrayColor,
    },
    headerTitle: () => <></>,
    headerLeft: () => (
      <TouchableOpacity onPress={() => onPrevious()}>
        <HeaderSideView>
          <HeaderLeftText>{title}</HeaderLeftText>
        </HeaderSideView>
      </TouchableOpacity>
    ),
  });
};
