/**
 * S3 업로드를 위한 함수 구현
 * 참고 글에서는 react-native-fs를 통해 base64로 읽기를 하지만, 설정이 까다로워서
 * expo filesystem을 통해 읽음.
 *
 * expo filesystem으로 읽을시 file:// scheme이 아닌 assets-library:// scheme 주소가 리턴되어,
 * 이를 변경해줄 라이브러리가 필요했고, expo-asset-utils가 완벽히 변형해주었다.
 *
 *  ref.
 *    https://medium.com/better-programming/upload-images-to-aws-s3-in-react-native-0-60-f20c547a963c
 *    https://docs.expo.io/versions/latest/sdk/filesystem/
 *    https://github.com/expo/expo-asset-utils
 */

import { encode, decode } from "base64-arraybuffer";
// import S3 from "aws-sdk/clients/s3";
import * as AWS from "aws-sdk";
import * as FileSystem from "expo-file-system";
import AssetUtils from "expo-asset-utils";

export const uploadImageOnS3 = async (file) => {
  const S3 = new AWS.S3({
    credentials: {
      accessKeyId: "AKIA5PZBCKBE7HOPKZHJ",
      secretAccessKey: "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",
    },
  });

  let contentType = "image/jpeg";
  let contentDeposition = 'inline;filename="' + file.name + '"';

  const { uri } = file;
  const base64 = await FileSystem.readAsStringAsync(uri, {
    encoding: FileSystem.EncodingType.Base64,
  });
  const arrayBuffer = decode(base64);

  const params = {
    Bucket: "feeeldapp",
    Key: file.name,
    Body: arrayBuffer,
    ContentDisposition: contentDeposition,
    ContentType: contentType,
  };

  let resS3 = undefined;
  await (async () => {
    resS3 = await S3.upload(params).promise();
  })();

  const { Location } = resS3;
  return Location;
};
