/*
 * 이미지 주소를 리사이즈를 위한 API 주소를 포함하여 바꿔주는 함수를 구현합니다.
 */
const RESIZE_API =
  "https://19u7t38m2l.execute-api.ap-northeast-2.amazonaws.com/latest";

const getResizedImageUrl = ({ uri, width, height }) => {
  return `${RESIZE_API}?url=${uri}&size=${width}x${height}`;
};

export default getResizedImageUrl;
