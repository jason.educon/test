/**
 * 작품 업로드 시 메타 데이터를 입력하는 에디터입니다.
 * 탭 형식으로 구성되어 있습니다.
 * - 저작권 정보 입력 탭(Copyright.js)
 * - 작품 상세 정보 입력 탭(DetailedInfo.js)
 */
import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import Copyright from "./Copyright";
import DetailedInfo from "./DetailedInfo";

import styled from "styled-components";
import GlobalStyles from "../../styles";

export default () => {
  const Tab = createMaterialTopTabNavigator();

  return (
    <Tab.Navigator
      initialRouteName="Copyright"
      tabBarOptions={{
        labelStyle: { fontFamily: "bold" },
        indicatorStyle: { backgroundColor: GlobalStyles.feeeldColor },
      }}
    >
      <Tab.Screen name="저작권 정보" component={Copyright} />
      <Tab.Screen name="상세 정보" component={DetailedInfo} />
    </Tab.Navigator>
  );
};
