/**
 * Photo Picker를 구현합니다.
 *
 * [!] 원래 코드에선 _processImageAsync라는 것을 구현해서 이미지 크기 조정 및 압축을 실행함.
 * 여기선 일단 제외 시킨다.
 * [!] ImageBrowser에서 내부적으로 생성하는 onSubmit 함수를 navHeader의 오른쪽 버튼에 연결 시키는 부분 주의.
 *
 * [!] 두 가지 모드로 실행
 *  init 모드는 최초로 에디터 진입 시도시 포토 피커를 거칠 때
 *  add 모드는 포토 에디터에서 이미지 추가 버튼을 눌러서 재진입할 때
 *    (포토 에디터로 되돌아 갔을 때) 유지해야하는 변수가 있기 때문에
 *      oldPhotos로 이전에 선택했던 이미지를,
 *      oldUriCaptionMap으로 이전에 작성했던 캡션 정보를 유지한다.
 *      (navigate할 때 파라미터로 주고 받음)
 *      특히, add 모드의 경우 추가로 선택한 이미지가 있다면 oldPhotos와 새로 선택한 이미지를 합쳐준다.
 *  그리고, 모드에 따라서 네비게이션 헤더의 뒤로가기를 어느 컴포넌트로 향하게 할지 세팅을 별도로 해준다.
 *
 * [!] imageCallback 이라는 함수는 imageBrowser 내부에서 결국 onSumbit 역할을 하게 됨. (복습 필요)
 *  따라서, 여기에서도 mode='init'과 mode='add'일 때 navigate 될 컴포넌트를 구분해줘야한다.
 *
 */
import React, { useState } from "react";
import { Text, View, StyleSheet } from "react-native";

import { setNavHeaderInPhotoPicker } from "../../utils/navHeader";

import GlobalStyles from "../../styles";

import { ImageBrowser } from "../../components/ImageBrowser";

const validModes = ["init", "add"];

const PhotoPicker = ({ route, navigation }) => {
  const { mode, oldPhotos, oldUriCaptionMap } = route.params;
  if (!validModes.includes(mode)) {
    alert("invalid mode(init or add)");
  }

  const onCancel =
    mode === "init"
      ? () => navigation.navigate("Home")
      : () =>
          navigation.navigate("PhotoEditor", {
            photos: oldPhotos,
            uriCaptionMap: oldUriCaptionMap,
          });

  setNavHeaderInPhotoPicker({
    navigation,
    onCancel,
    onSubmit: () => {
      alert("이미지를 선택해주세요");
    },
  });

  const imagesCallback = async (assetsInfoPromiseAll) => {
    // onSubmit으로 결국 들어갈 함수
    const photos = await assetsInfoPromiseAll;
    const finalPhotos = [];
    for (let photo of photos) {
      finalPhotos.push({
        uri: photo.uri,
        name: photo.filename,
        type: "image/jpg",
      });
    }
    if (photos.length === 0) {
      alert("이미지를 선택해주세요");
      return;
    }
    if (mode == "init") {
      navigation.navigate("PhotoEditor", { photos: finalPhotos });
    } else if (mode == "add") {
      navigation.navigate("PhotoEditor", {
        photos:
          finalPhotos.length > 0 ? [...oldPhotos, ...finalPhotos] : oldPhotos,
        uriCaptionMap: oldUriCaptionMap,
      });
    }
  };

  const renderSelectedPhoto = (number) => (
    <View style={styles.countBadge}>
      <Text style={styles.countBadgeText}>{number}</Text>
    </View>
  );

  const updateHandler = (count, onSubmit) => {
    setNavHeaderInPhotoPicker({ navigation, onCancel, onSubmit });
  };

  return (
    <View style={[styles.flex, styles.container]}>
      <ImageBrowser
        max={10}
        loadCount={40}
        onChange={updateHandler}
        callback={imagesCallback}
        emptyStayComponent={null}
        renderSelectedComponent={renderSelectedPhoto}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    position: "relative",
  },
  emptyStay: {
    textAlign: "center",
  },
  countBadge: {
    paddingHorizontal: 8.6,
    paddingVertical: 5,
    borderRadius: 15,
    position: "absolute",
    right: 3,
    bottom: 3,
    justifyContent: "center",
    backgroundColor: GlobalStyles.feeeldColor,
  },
  countBadgeText: {
    fontWeight: "bold",
    alignSelf: "center",
    padding: "auto",
    color: "#ffffff",
  },
});

export default PhotoPicker;
