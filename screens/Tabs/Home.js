/**
 * 썸네일 슬라이더 (components/ThumbnailSlider.js)를 포함하고 있다.
 * 썸네일 슬라이더는 네비게이션을 전달 받아 (누가 전달해주는 건지 확인 필요)
 * 썸네일 클릭시 뷰어로 라우팅 해준다.
 */
import React, { useState } from "react";
import { ScrollView, View, Text, StyleSheet, AsyncStorage } from "react-native";

import { gql } from "apollo-boost";
import { useQuery } from "react-apollo-hooks";

import styled from "styled-components";
import GlobalStyle from "../../styles";

import ThumbnailSlider from "../../components/ThumbnailSlider";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";

import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const ThumbSlider = styled.View`
  margin-bottom: 20px;
`;

export const GET_POSTS = gql`
  {
    hotPosts(offset: 0) {
      id
      title
      imgList
      User {
        nick
        profileImage
      }
      Likes {
        id
      }
      version
      viewCount
    }
    fbcPosts(yearmonth: "2020-08") {
      id
      title
      imgList
      User {
        nick
        profileImage
      }
      Likes {
        id
      }
      version
      viewCount
    }
    latestPosts(offset: 0) {
      id
      title
      imgList
      User {
        nick
        profileImage
      }
      Likes {
        id
      }
      version
      viewCount
    }
    postsByLikes(offset: 0) {
      id
      title
      imgList
      User {
        nick
        profileImage
      }
      Likes {
        id
      }
      version
      viewCount
    }
    postsByComments(offset: 0) {
      id
      title
      imgList
      User {
        nick
        profileImage
      }
      Likes {
        id
      }
      version
      viewCount
    }
  }
`;

export default ({ navigation }) => {
  setNavHeader({ navigation, visible: true });

  const { loading, data, refetch } = useQuery(GET_POSTS);
  return (
    <FloatingButtonContainer>
      <ScrollView scrollEventThrottle={16} showsVerticalScrollIndicator={false}>
        {loading ? (
          <View>
            <Text>Loading...</Text>
          </View>
        ) : (
          data &&
          data.hotPosts && (
            <>
              <ThumbSlider>
                <ThumbnailSlider
                  navigation={navigation}
                  title="필디에서 핫한 필보드차트 HOT 10"
                  mode="1+"
                  posts={data.fbcPosts}
                />
              </ThumbSlider>
              <ThumbSlider>
                <ThumbnailSlider
                  navigation={navigation}
                  title="따끈 따끈 최근에 올라온 작품들"
                  mode="1+"
                  posts={data.latestPosts}
                />
              </ThumbSlider>
              <ThumbSlider>
                <ThumbnailSlider
                  navigation={navigation}
                  title="오리지날 유저들이 좋아하는 작업들"
                  mode="1+"
                  posts={data.postsByLikes}
                />
              </ThumbSlider>
              <ThumbSlider>
                <ThumbnailSlider
                  navigation={navigation}
                  title="많은 필더들이 소통한 작업들"
                  mode="1+"
                  posts={data.hotPosts}
                />
              </ThumbSlider>
              <ThumbSlider>
                <ThumbnailSlider
                  navigation={navigation}
                  title="요즘 떠오르는 작업들"
                  mode="1+"
                  posts={data.postsByComments}
                />
              </ThumbSlider>
              <ThumbSlider>
                <ThumbnailSlider
                  navigation={navigation}
                  title="작가와 이야기 나누고 싶은 작업들"
                  mode="1+"
                  posts={data.postsByComments}
                />
              </ThumbSlider>
              <ThumbSlider>
                <ThumbnailSlider
                  navigation={navigation}
                  title="인기 급상승 중인 작품들"
                  mode="1+"
                  posts={data.postsByComments}
                />
              </ThumbSlider>
            </>
          )
        )}
      </ScrollView>
      <FloatingButton
        icon="feeeld"
        onPress={() => navigation.navigate("PhotoPicker", {mode: "init"})}
        direction="right"
        visible={true}
      />
    </FloatingButtonContainer>
  );
};
