/**
 * 썸네일 슬라이더 컴포넌트에서 썸네일 클릭시
 * 라우팅 되는 뷰어 스크린이다.
 *
 * Post.js 컴포넌트를 호출하여 포스트의 내용을 보여준다.
 *
 * [!] showFloatingButton 스테이트를 정의하고, Post.js => Comments.js 까지 전달해준다.
 * 댓글 작성시 하단 버튼을 제어하기 위해서
 *
 * [!] 추후에 별도의 폴더를 생성해서 정리할 필요가 있음
 */
import React, { useState } from "react";
import { useQuery } from "react-apollo-hooks";
import { gql } from "apollo-boost";

import Post from "../../components/Post";
import { ScrollView, View, Text } from "react-native";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const POST_DETAIL = gql`
  query getPost($postId: String!) {
    getPost(postId: $postId) {
      title
      content
      imgList
      viewCount
      mainImgIdx
      Hashtags {
        title
      }
      Likes {
        id
      }
      User {
        id
        nick
        profileImage
      }
    }
  }
`;

export default ({ route, navigation }) => {
  setNavHeader({ navigation, visible: true });
  const { postId } = route.params;
  const { loading, data } = useQuery(POST_DETAIL, {
    variables: { postId },
  });
  const [showFloatingButton, setShowFloatingButton] = useState(true);

  return (
    <FloatingButtonContainer>
      <KeyboardAwareScrollView>
        {loading ? (
          <Text>Loading...</Text>
        ) : (
          data &&
          data.getPost && (
            <Post
              {...data.getPost}
              postId={postId}
              setShowFloatingButton={setShowFloatingButton}
            />
          )
        )}
      </KeyboardAwareScrollView>
      <FloatingButton
        icon="left"
        onPress={() => navigation.goBack()}
        direction="left"
        visible={showFloatingButton}
      />
      <FloatingButton
        icon="heart"
        onPress={() => alert("Heart Button!")}
        direction="right"
        visible={showFloatingButton}
      />
    </FloatingButtonContainer>
  );
};
