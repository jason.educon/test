/**
 * 유저 탭 클릭시 라우팅 되는 스크린이다.
 *
 * [!] 이 스크린에서 최초로 보여주는 AuthHome은 네비게이션의 라우팅에 의해 불려지지 않으므로
 * navigation을 직접 넣어주어야 했다.
 * navigation 전달 부분을 제거하고 실행 시켜 보면 화면이 깨짐과 동시에 링크도 동작하지 않는다.
 */
import React, { useState } from "react";
import { useQuery } from "react-apollo-hooks";
import { gql } from "apollo-boost";

import { View, Text, Dimensions } from "react-native";

import styled from "styled-components";
import GlobalStyles from "../../styles";
import { MaterialIcons } from "@expo/vector-icons";

import { useIsLoggedIn } from "../../AuthContext";

import getResizedImageUrl from "../../utils/ImageUrl";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";

import constants from "../../constants";
import AuthHome from "../Auth/AuthHome";
import Settings from "../../components/Settings";
import { Touchable } from "../../components/GlobalStyledComponent";

const USER_PROFILE = gql`
  query {
    getUser (id: "ckb9f9pggvotm0963nw5rhpik") {
      nickname
      imgUrl
      phone
      provider
      email
    }
  }
`;

const { width, height } = Dimensions.get("window");

const UserProfileView = styled.SafeAreaView`
  flex: 1;
  background-color: white;
  padding-top: ${constants.statusBarHeight}px;
`;

const ProfileContainer = styled.View`
  flex-direction: row;
  margin: 10px;
`;

const UserImage = styled.View`
  width: 50px;
  height: 50px;
  align-items: center;
  justify-content: center;
  border-radius: 50px;
  background-color: grey;
`;

const UserProfile = styled.View`
  flex: 9;
  margin: 10px;
`;

const UserNickName = styled.Text`
  ${GlobalStyles.h3};
`;

const UserEmail = styled.Text`
  ${GlobalStyles.h5};
`;

const Setting = styled.View`
  flex: 1;
  margin: 10px;
  align-items: center;
  justify-content: center;
`;

export default ({ navigation }) => {
  setNavHeader({ navigation, visible: false });
  const [showSettings, setShowSettings] = useState(false);
  const { data } = useQuery(USER_PROFILE);

  const isLoggedIn = useIsLoggedIn();


  const toggleSettings = () => {
    setShowSettings(!showSettings);
  };

  return isLoggedIn ? (
    <UserProfileView>
      <ProfileContainer>
        <UserImage
          /* source={{
            uri: getResizedImageUrl({
              uri: post.User.profileImage,
              width: 50,
              height: 50,
              }),
          }}
        */
        />
        <UserProfile>
        <UserNickName>USER PROFILE</UserNickName>
        <UserEmail>USER Email</UserEmail>
        </UserProfile>
        <Setting>
          <Touchable onPress={toggleSettings}>
            <MaterialIcons name="settings" size={24} color="black" />
          </Touchable>
        </Setting>
        
      </ProfileContainer>
      
      {/* Logout => [!] 스타일 수정 필요 */}
      
      {/* Setting 버튼 => 버튼 클릭시 shoSettings 변수 변함 (true<->false)
       * [!] toggle이 될 경우 설정 버튼을 없앨까? 아니면 X 표시로 바꿔야 하나?
       */}
      

      {/* Setting 버튼 선택시 나오는 화면
       * [!] 버튼이 선택 안됐을 경우 '마이페이지' 보여주기 (추후 Sprint?)
       */}
      {showSettings ? <Settings navigation={navigation}></Settings> : <></>}
      <FloatingButton
            icon="left"
            onPress={() => navigation.goBack()}
            direction="left"
            visible={true}
          />
    </UserProfileView>
  ) : (
    <AuthHome navigation={navigation} />
  );
};
