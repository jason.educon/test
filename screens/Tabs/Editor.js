/**
 * 메인 에디터입니다.
 * 이미지 편집으로 진입이 가능합니다.
 * 텍스트 그룹을 추가하여 본문 작성이 가능합니다.
 * 랜덤 질문 변경을 위해 질문을 가지고 있습니다.
 *
 * [!] iPhone에선 TitleTextArea에 flex 1을 걸지 않을 경우 첫 몇 글자가 왼쪽으로 밀리는 버그가 생긴다.
 *
 * [!] uriCaptionMap은 PhotoEditor에서 캡션을 세팅하고 돌아왔을 때 세팅되어 있다. 상태를 유지하기 위해 전달하고 있음.
 */
import React, { useState, useRef } from "react";
import { Text, KeyboardAvoidingView, Alert } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { gql } from "apollo-boost";
import { useMutation } from "react-apollo-hooks";
//import { useMutation } from "@apollo/react-hooks";

import styled from "styled-components";
import GlobalStyles from "../../styles";
import { FontAwesome, MaterialCommunityIcons } from "@expo/vector-icons";

import { setNavHeaderInEditor } from "../../utils/navHeader";
import TextGroup from "../../components/TextGroup";
import useInput from "../../hooks/useInput";

import FloatingButton from "../../components/FloatingButton";
import {
  FloatingButtonContainer,
  Touchable,
} from "../../components/GlobalStyledComponent";

import SelectedPhotoSlider from "../../components/SelectedPhotoSlider";
import { questions } from "../../components/Questions";

import { uploadImageOnS3 } from "../../utils/s3";
import { GET_POSTS } from "../Tabs/Home";

const UPLOAD = gql`
  mutation createPost(
    $title: String!
    $content: String!
    $files: [String!]!
    $captions: [String!]!
    $tags: [String!]!
  ) {
    createPost(
      title: $title
      content: $content
      files: $files
      captions: $captions
      tags: $tags
    ) {
      id
      title
    }
  }
`;

const Border = styled.View`
  border-bottom-color: ${GlobalStyles.editorBorderColor};
  border-bottom-width: 1px;
  margin: 15px;
`;

const ImageContainer = styled.View`
  background-color: white;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
`;

const TitleContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 0 20px;
  margin-bottom: 5px;
`;

const TitleTextArea = styled.TextInput`
  ${GlobalStyles.h1};
  flex: 1;
  margin-top: 20px;
  margin-left: 5px;
  padding: 5px;
  height: 50px;
  text-align-vertical: center;
`;

const DescriptionContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 0 20px;
  margin-bottom: 5px;
`;

const DescriptionInput = styled.TextInput`
  flex: 1;
  ${GlobalStyles.h2};
  margin-top: 10px;
  margin-left: 5px;
  padding: 5px;
  min-height: 300px;
  height: auto;
  max-height: 500px;
  text-align-vertical: center;
`;

const TagContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 0 20px;
  margin-bottom: 5px;
`;

const TagInput = styled.TextInput`
  flex: 1;
  ${GlobalStyles.h2};
  margin-top: 5px;
  margin-left: 5px;
  padding: 5px;
  height: 50px;
  text-align-vertical: center;
`;

const Label = styled.Text`
  font-size: 15px;
  font-family: "medium";
  color: ${GlobalStyles.darkGrayColor};
  margin-left: 5px;
`;

const ImageThumbSlider = styled.View`
  margin-bottom: 15px;
`;

const shuffle = (arr) => {
  for (let i = arr.length; i; i -= 1) {
    const j = Math.floor(Math.random() * i);
    const x = arr[i - 1];
    arr[i - 1] = arr[j];
    arr[j] = x;
  }
};

export default ({ route, navigation }) => {
  const { photos, uriCaptionMap } = route.params;
  const [isLoading, setIsLoading] = useState(false);
  const [path, setPath] = useState(null);

  const [title, setTitle] = useState();
  const [textGroupInfoList, setTextGroupInfoList] = useState([]);
  const [content, setContent] = useState();
  const [tags, setTags] = useState();

  const [createPost, { data, loading }] = useMutation(UPLOAD);

  // const uploadMutation = useMutation(UPLOAD, {
  //   variables: {
  //     title: title,
  //     content: content,
  //   },
  // });

  const [inputActivateA, setInputActivateA] = useState();
  const [inputActivateB, setInputActivateB] = useState();
  const [inputActivateC, setInputActivateC] = useState();

  const onInputActivateA = () => {
    setInputActivateA(!inputActivateA);
  };
  const onInputActivateB = () => {
    setInputActivateB(!inputActivateB);
  };
  const onInputActivateC = () => {
    setInputActivateC(!inputActivateC);
  };

  const addTextGroupInfo = () => {
    const tmp = {
      title: "터치하여 직접 작성하거나, \n▶ 버튼을 눌러 랜덤 생성해 보세요.",
      content: "",
    };
    textGroupInfoList.push(tmp);
    setTextGroupInfoList(textGroupInfoList.slice());
    //shuffle(questions);
  };

  const changeQuestion = (index) => {
    shuffle(questions);
    textGroupInfoList[index].title = questions[0];
    setTextGroupInfoList(textGroupInfoList.slice());
  };

  const removeTextGroup = (index) => {
    textGroupInfoList.splice(index, 1);
    setTextGroupInfoList(textGroupInfoList.slice());
  };

  const onPrevious = () => {
    // EditorMode: "create" 에선 이미 이전 스택에 PhotoEditor가 유지되고 있기 때문에
    // 별도의 것(photos, uriCaptionMap)을 전달하지 않아도 된다.
    // 오히려 전달하는 경우 에러가 난다.
    navigation.navigate("PhotoEditor", {
      // photos,
      // uriCaptionMap
    });
    // 미래에 EditorMode: "update" 에선, editor 이후에 나타난다.
    // 이렇게 되면, editor 스택 위에 올라가기 때문에
    // photos, uriCaptionMap을 전달해도 될듯하다.
  };

  const onSubmit = async () => {
    // alert("게시로 연결");
    if (title == null || photos.length == 0) {
      Alert.alert(
        "사진이 적어도 한 장 이상, 제목이 작성되어 있어야 업로드 가능합니다."
      );
    }
    const files = [];
    const captions = [];

    try {
      setIsLoading(true);
      for (let photo of photos) {
        // console.log(photo); // Obejct {"name": "", "type": "", "uri": ""}
        const caption = uriCaptionMap[photo.uri];
        const path = await uploadImageOnS3(photo);

        captions.push(caption);
        files.push(path);
        console.log("caption: ", caption);
        console.log("path: ", path);
        console.log("test");
      }

      // String으로 이루어진 tag를 '#'을 중심으로 잘라 tagList로 만들기
      let tmpTags = tags.split("#");
      tmpTags = tmpTags.map(Function.prototype.call, String.prototype.trim);
      const tagList = tmpTags.filter(function (e) {
        return e != "";
      });

      const { data, loading } = await createPost({
        variables: {
          title: title,
          content: content,
          files: files,
          captions: captions,
          tags: tagList,
        },
      });

      if (data) {
        //navigation.popToTop();
        const postId = data.createPost.id;
        console.log(postId);
        //navigation.navigate("Viewer", { postId: postId });
        // [!] 추후 위의 코드로 업로드 완료시 바로 Post 뷰 보여줄 예정

        navigation.popToTop();
        Alert.alert("업로드 완료!");
      } else {
        console.log(data);
      }

      setIsLoading(false);
    } catch (e) {
      console.log(e);
    } finally {
    }
  };

  setNavHeaderInEditor({
    navigation,
    onPrevious,
    onSubmit,
  });

  return (
    <FloatingButtonContainer>
      <KeyboardAwareScrollView
        scrollEventThrottle={16}
        showsVerticalScrollIndicator={false}
      >
        <ImageContainer>
          <ImageThumbSlider>
            <SelectedPhotoSlider photos={photos} />
          </ImageThumbSlider>
          {/* 
          <Touchable
            onPress={() =>
              navigation.navigate("PhotoEditor", { photos, uriCaptionMap })
            }
            style={{ alignItems: "center", marginTop: 15, marginBottom: 15 }}
          >
            <AddingText>캡션 입력과 순서 변경은 버튼 클릭!</AddingText>
            <MaterialCommunityIcons
              name="message-image"
              size={50}
              color={GlobalStyles.feeeldColor}
            />
          </Touchable>
          */}
        </ImageContainer>
        <TitleContainer>
          <TitleTextArea
            value={title}
            placeholder="제목을 입력해 주세요"
            placeholderTextColor="rgba(200,200,200,0.7)"
            scrollEnabled={false}
            clearButtonMode="always"
            selectionColor={GlobalStyles.feeeldColor}
            onChangeText={setTitle}
            onFocus={onInputActivateA}
            onBlur={onInputActivateA}
            style={[
              inputActivateA
                ? { borderWidth: 1, borderColor: "#55F3B3", borderRadius: 5 }
                : { backgroundColor: "white" },
            ]}
          />
        </TitleContainer>
        <Border></Border>
        <DescriptionContainer>
          <DescriptionInput
            value={content}
            placeholder="내용을 입력해 주세요"
            placeholderTextColor="rgba(200,200,200,0.7)"
            selectionColor={GlobalStyles.feeeldColor}
            multiline={true}
            onChangeText={setContent}
            onFocus={onInputActivateB}
            onBlur={onInputActivateB}
            style={[
              inputActivateB
                ? { borderWidth: 1, borderColor: "#55F3B3", borderRadius: 5 }
                : { backgroundColor: "white" },
            ]}
          />
        </DescriptionContainer>
        <Border></Border>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
        >
          <TagContainer>
            <TagInput
              value={tags}
              placeholder="태그(#)를 입력해 주세요"
              placeholderTextColor="rgba(200,200,200,0.7)"
              scrollEnabled={false}
              selectionColor={GlobalStyles.feeeldColor}
              clearButtonMode="always"
              keyboardType="twitter"
              onChangeText={setTags}
              onFocus={onInputActivateC}
              onBlur={onInputActivateC}
              style={[
                inputActivateC
                  ? { borderWidth: 1, borderColor: "#55F3B3", borderRadius: 5 }
                  : { backgroundColor: "white" },
              ]}
            />
          </TagContainer>
          <Border></Border>
          {/* 
        {textGroupInfoList.map((info, index) => {
          return (
            <TextGroup
              key={index}
              index={index}
              textGroupInfoList={textGroupInfoList}
              setTextGroupInfoList={setTextGroupInfoList}
              changeQuestion={changeQuestion}
              removeTextGroup={removeTextGroup}
            />
          );
        })}

        <Touchable
          onPress={addTextGroupInfo}
          style={{ alignItems: "center", marginTop: 20, marginBottom: 300 }}
        >
          <AddingText style={{ alignItems: "center" }}>
            {textGroupInfoList.length == 0
              ? `OO님, 이 작품은 어떤 작품인가요?\n버튼을 눌러 생각을 입력해 주세요!`
              : `OO님의 생각을 더 알고싶어요!\n버튼을 눌러 생각을 입력해 주세요!`}
          </AddingText>
          <MaterialCommunityIcons
            name="message-processing"
            size={50}
            color={GlobalStyles.feeeldColor}
          />
        </Touchable>
        */}
        </KeyboardAvoidingView>
      </KeyboardAwareScrollView>
    </FloatingButtonContainer>
  );
};
