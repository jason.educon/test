/**
 * 포토 에디터를 구현합니다.
 *
 * 포토 에디터에서는 이미지 순서의 변경, 삭제, 캡션 추가 및 수정 등의 작업을 할 수 있다.
 *
 * [!] 현재 리액트의 랜더링 문제로 드래거블 이벤트 작동시 깜빡이는 문제가 있다.
 * [!] ios에서 키보드가 TextInput을 덮는 문제는 KeyboardAvoidingView를 쓰면 쉽게 해결된다.
 * [!] TouchableWithoutFeedback을 사용할 때는, 자식을 <View>로 한 번 감싸줘야 했다.
 *
 * [todo] ref를 close 해줘야 하나? swipeable 예시를 보다보니 그런 부분이 있어서..
 *
 * [!] uriCaptionMap은
 *  이미지 추가 버튼 클릭으로 PhotoPicker를 거쳐서 되돌아 왔을 때
 *  혹은 Editor로 돌아갔다가 다시 진입했을 때 전달된다.
 *  상태를 유지하기 위해 네비게이트 될 때 주고 받는다.
 *
 * [bugfix] onNext에서 Editor로 넘어가기 전에, 캡션 매핑이 바뀌어버린다.
 * 드래그 해서 순서 변경 후 Editor를 갔다가 PhotoEditor로 돌아왔다가 다시 Editor로 가는 순간
 * 매핑이 바뀌는 이상한 문제가 있다. 플로우를 변경하면서 스택에서 Editor아래에 PhotoEditor가 위치하는 바람에
 * Editor에서 PhotoEditor로 굳이 데이터를 전달하지 않아도 데이터가 유지가 되는데 여기서 억지로 넣으려하면서
 * 버그가 발생한듯하다. 생성 모드에서 Editor에서 PhotoEditor로 돌아올 때는 데이터 전달하지 않음으로써 해결.
 * Editor.js의 onPrevious를 참고.
 */
import React, { useState, useRef } from "react";
import {
  Text,
  Button,
  View,
  ImageBackground,
  Alert,
  KeyboardAvoidingView,
  TouchableWithoutFeedback
} from "react-native";

import { Feather, FontAwesome } from "@expo/vector-icons";
import { setNavHeaderInPhotoEditor } from "../../utils/navHeader";

import useInput from "../../hooks/useInput";

import { useHeaderHeight } from "@react-navigation/stack";
import DraggableFlatList from "react-native-draggable-flatlist";
import Modal from "react-native-modal";

import {
  FloatingButtonContainer,
  Touchable
} from "../../components/GlobalStyledComponent";

import constants from "../../constants";

import styled from "styled-components";
import GlobalStyles from "../../styles";

const CircleView = styled.View`
  width: 25px;
  height: 25px;
  background-color: rgba(255, 255, 255, 0.7);
  border-radius: 50px;
  align-items: center;
  justify-content: center;
`;

const AddImgView = styled.View`
  margin: 15px;
  height: 150px;
  border-radius: 20px;
  background-color: ${GlobalStyles.grayColor};
  align-items: center;
  justify-content: center;
`;

const CaptionInputView = styled.View`
  flex: 9;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.3);
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  padding-left: 25px;
`;

const CaptionText = styled.Text`
  color: white;
  font-size: 20px;
  padding: 10px;
  text-align: center;
  font-family: "bold";
`;

const CaptionBtnView = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.3);
  border-top-right-radius: 20px;
  border-bottom-right-radius: 20px;
`;

const ModalView = styled.View`
  margin: 10px;
  padding: 10px;
  background-color: white;
  border-radius: 40px;
  height: 50px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const ModalCaptionInput = styled.TextInput`
  flex: 5;
  padding: 0;
  padding-left: 5px;
  font-family: "regular";
`;

const ModalCaptionCounter = styled.Text`
  margin-right: 5px;
  color: ${GlobalStyles.grayColor};
  font-family: "regular";
`;

const ModalCaptionBtn = styled.View`
  background-color: ${GlobalStyles.feeeldColor};
  border-radius: 20px;
  color: white;
  width: 40px;
  height: 30px;
  padding: 5px;
  align-items: center;
  justify-content: center;
`;

export default ({ route, navigation }) => {
  let { photos, uriCaptionMap } = route.params;

  const [curPhotos, setCurPhotos] = useState(photos);
  const [isModalVisible, setModalVisible] = useState(false);
  const [captionIdx, setCaptionIdx] = useState(0);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const headerHeight = useHeaderHeight();
  const deviceHeight = constants.height;

  const itemMargin = 10;
  const itemHeight = (deviceHeight - headerHeight) / 4.5 - 2 * itemMargin;

  const inputRefsMap = {};
  const inputHookMap = {};
  for (let photo of photos) {
    inputRefsMap[photo.uri] = useRef();
    // editorMode: "update" 일 경우에만 아래를 실행
    // const defaultValue =
    //  uriCaptionMap && photo.uri in uriCaptionMap
    //    ? uriCaptionMap[photo.uri]
    //    : "";
    inputHookMap[photo.uri] = useInput("");
  }

  const extractUriCaptionMap = () => {
    const uriCaptionMap = {};
    for (let photo of photos) {
      uriCaptionMap[photo.uri] = inputHookMap[photo.uri].value;
    }
    return uriCaptionMap;
  };

  const onPrevious = () => {
    navigation.navigate("PhotoPicker", {
      mode: "init"
    });
  };

  const onNext = () => {
    navigation.navigate("Editor", {
      photos: curPhotos,
      uriCaptionMap: extractUriCaptionMap()
    });
  };

  setNavHeaderInPhotoEditor({
    navigation,
    onNext,
    onPrevious
  });

  const addCaption = index => {
    setModalVisible(true);
    setCaptionIdx(index);
  };

  const removeItem = index => {
    Alert.alert(
      "이미지 삭제",
      "이미지를 삭제합니다.",
      [
        {
          text: "취소"
        },
        {
          text: "삭제",
          onPress: () => {
            const clonedPhotos = curPhotos.slice();
            clonedPhotos.splice(index, 1);
            setCurPhotos(clonedPhotos);
          }
        }
      ],
      { cancelable: false }
    );
  };

  const renderItem = ({ item, index, drag, isActive }) => {
    return (
      <TouchableWithoutFeedback
        style={{
          position: "relative",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "row"
        }}
        onLongPress={drag}
        delayLongPress={200}
      >
        <View
          style={{
            margin: 0
          }}
        >
          <ImageBackground
            source={{ uri: item.uri }}
            style={{
              flex: 1,
              margin: itemMargin,
              marginBottom: 0,
              height: itemHeight,
              resizeMode: "cover",
              justifyContent: "center",
              flexDirection: "row"
            }}
            imageStyle={{
              borderRadius: 20,
              borderColor: isActive ? GlobalStyles.feeeldColor : GlobalStyles.grayColor,
              borderWidth: isActive ? 4 : 0
            }}
          >
            <CaptionInputView>
              <TouchableWithoutFeedback onPress={() => addCaption(index)}>
                {inputHookMap[item.uri].value === "" ? (
                  <CaptionText
                    style={{
                      fontSize: 15,
                      color: "#B5B5B5"
                    }}
                  >
                    캡션을 입력해주세요
                  </CaptionText>
                ) : (
                  <CaptionText>{inputHookMap[item.uri].value}</CaptionText>
                )}
              </TouchableWithoutFeedback>
            </CaptionInputView>

            <CaptionBtnView>
              <TouchableWithoutFeedback
                onPress={() => removeItem(index)}
                style={{ position: "absolute", top: 10, right: 10 }}
              >
                <CircleView>
                  <Feather name="trash-2" size={15} color="rgba(1,1,1,0.5)" />
                </CircleView>
              </TouchableWithoutFeedback>
            </CaptionBtnView>
          </ImageBackground>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <FloatingButtonContainer>
      <View style={{ flex: 1 }}>
        <DraggableFlatList
          data={curPhotos}
          renderItem={renderItem}
          lastItem={renderItem}
          keyExtractor={(item, index) => `draggable-item-${index}`}
          onDragEnd={({ data }) => setCurPhotos(data)}
        ></DraggableFlatList>
      </View>
      {curPhotos && captionIdx < curPhotos.length ? (
        <Modal
          isVisible={isModalVisible}
          propagateSwipe={true}
          onSwipeComplete={toggleModal}
          onBackdropPress={toggleModal}
          swipeDirection={["left", "right", "down"]}
          backdropOpacity={0.3}
          style={{ justifyContent: "flex-end", margin: 0 }}
        >
          <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
          >
            <ModalView>
              <ModalCaptionInput
                ref={inputRefsMap[curPhotos[captionIdx].uri]}
                onChangeText={inputHookMap[curPhotos[captionIdx].uri].onChange}
                value={inputHookMap[curPhotos[captionIdx].uri].value}
                placeholderTextColor={GlobalStyles.lightGrayColor}
                placeholder="캡션을 입력해주세요."
                autoFocus={true}
                maxLength={30}
              />
              <ModalCaptionCounter>
                {inputHookMap[curPhotos[captionIdx].uri].value.length}/30
              </ModalCaptionCounter>
              <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
                <ModalCaptionBtn>
                  <Text style={{ fontFamily: "medium" }}>입력</Text>
                </ModalCaptionBtn>
              </TouchableWithoutFeedback>
            </ModalView>
          </KeyboardAvoidingView>
        </Modal>
      ) : (
        <></>
      )}
    </FloatingButtonContainer>
  );
};
