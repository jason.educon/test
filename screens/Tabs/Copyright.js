/**
 * 저작권 정보를 입력하는 탭입니다.
 *
 */

import React, { useState } from "react";
import {
  Text,
  View,
  Button,
  Alert,
  TouchableHighlight,
  StyleSheet,
} from "react-native";
import { AppLoading } from "expo";

import { FontAwesome5 } from "@expo/vector-icons";

import styled from "styled-components";
import styles from "../../styles";
import GlobalStyles from "../../styles";
import {
  FloatingButtonContainer,
  Touchable,
} from "../../components/GlobalStyledComponent";

import CopyrightModal from "../../components/CopyrightModal";

import {
  useFonts,
  NotoSansKR_100Thin,
  NotoSansKR_300Light,
  NotoSansKR_400Regular,
  NotoSansKR_500Medium,
  NotoSansKR_700Bold,
  NotoSansKR_900Black,
} from "@expo-google-fonts/noto-sans-kr";

const CopyrightHeader = styled.View`
  padding-left: 20px;
  margin-top: 30px;
  margin-bottom: 20px;
`;

const CopyrightHeaderText = styled.Text`
  font-size: 20px;
`;

const RequiredText = styled.Text`
  font-size: 12px;
  padding-top: 5px;
  color: #50d6a8;
`;

const CopyrightButton = styled.View`
  height: 135px;
  flex-direction: row;
  justify-content: center;
`;

const BYButton = styled.View`
  width: 130px;
  height: 130px;
  border-top-left-radius: 60px;
  margin: 2.5px;
  justify-content: center;
  align-items: center;
`;
const NCButton = styled.View`
  width: 130px;
  height: 130px;
  border-top-right-radius: 60px;
  margin: 2.5px;
  justify-content: center;
  align-items: center;
`;
const SAButton = styled.View`
  width: 130px;
  height: 130px;
  border-bottom-left-radius: 60px;
  margin: 2.5px;
  justify-content: center;
  align-items: center;
`;
const NDButton = styled.View`
  width: 130px;
  height: 130px;
  border-bottom-right-radius: 60px;
  margin: 2.5px;
  justify-content: center;
  align-items: center;
`;

const BYButtonText = styled.Text`
  font-size: 15px;
  color: #fff;
`;

const NCButtonText = styled.Text`
  font-size: 15px;
  color: #fff;
`;

const SAButtonText = styled.Text`
  font-size: 15px;
  color: #fff;
`;

const NDButtonText = styled.Text`
  font-size: 15px;
  color: #fff;
`;

const CopyrightDetail = styled.View`
  margin-top: 40px;
`;

const CopyrightDetailText = styled.Text`
  font-size: 12px;
  color: grey;
  text-decoration: underline;
  padding-left: 20px;
`;

const Border = styled.View`
  border-bottom-color: ${GlobalStyles.editorBorderColor};
  border-bottom-width: 1px;
  margin: 20px;
`;

const CopyrightTitle = styled.Text`
  font-size: 15px;
  text-align: center;
  margin-bottom: 5px;
`;

const CopyrightDescription = styled.Text`
  font-size: 12px;
  text-align: center;
  margin: 15px;
`;

const Copyright = () => {
  let [fontsLoaded] = useFonts({
    NotoSansKR_400Regular,
    NotoSansKR_700Bold,
    NotoSansKR_900Black,
  });
  const [visibleModal, setVisibleModal] = useState(false);

  const closeModal = () => {
    setVisibleModal(false);
  };

  const [byState, setBYState] = useState(false);
  const [ncState, setNCState] = useState(false);
  const [saState, setSAState] = useState(false);
  const [ndState, setNDState] = useState(false);

  const onClickBY = () => {
    if (byState) {
      setBYState(false);
      setNCState(false);
      setSAState(false);
      setNDState(false);
    } else {
      setBYState(true);
    }
  };

  const onClickNC = () => {
    if (ncState) {
      setNCState(false);
    } else {
      setBYState(true);
      setNCState(true);
    }
  };

  const onClickSA = () => {
    if (saState) {
      setSAState(false);
    } else {
      setSAState(true);
      setBYState(true);
      setNDState(false);
    }
  };

  const onClickND = () => {
    if (ndState) {
      setNDState(false);
    } else {
      setNDState(true);
      setBYState(true);
      setSAState(false);
    }
  };

  const getCopyrightTitle = () => {
    if (!byState && !ncState && !saState && !ndState) {
      return (
        <Text>
          <FontAwesome5 name="tired" size={30} color={"black"} />
          {"\n"}
          {"All Rights Reserved"}
        </Text>
      );
    } else if (byState && !ncState && !saState && !ndState) {
      return (
        <Text>
          <FontAwesome5 name="creative-commons-by" size={30} color={"black"} />
          {"\n"}
          {"저작자표시"}
        </Text>
      );
    } else if (byState && ncState && !saState && !ndState) {
      return (
        <Text>
          <FontAwesome5 name="creative-commons-by" size={30} color={"black"} />{" "}
          <FontAwesome5 name="creative-commons-nc" size={30} color={"black"} />
          {"\n"}
          {"저작자표시-비영리"}
        </Text>
      );
    } else if (byState && !ncState && !saState && ndState) {
      return (
        <Text>
          <FontAwesome5 name="creative-commons-by" size={30} color={"black"} />{" "}
          <FontAwesome5 name="creative-commons-nd" size={30} color={"black"} />
          {"\n"}
          {"저작자표시-변경금지"}
        </Text>
      );
    } else if (byState && !ncState && saState && !ndState) {
      return (
        <Text>
          <FontAwesome5 name="creative-commons-by" size={30} color={"black"} />{" "}
          <FontAwesome5 name="creative-commons-sa" size={30} color={"black"} />
          {"\n"}
          {"저작자표시-동일조건변경허락"}
        </Text>
      );
    } else if (byState && ncState && saState && !ndState) {
      return (
        <Text>
          <FontAwesome5 name="creative-commons-by" size={30} color={"black"} />{" "}
          <FontAwesome5 name="creative-commons-nc" size={30} color={"black"} />{" "}
          <FontAwesome5 name="creative-commons-sa" size={30} color={"black"} />
          {"\n"}
          {"저작자표시-비영리-동일조건변경허락"}
        </Text>
      );
    } else if (byState && ncState && !saState && ndState) {
      return (
        <Text>
          <FontAwesome5 name="creative-commons-by" size={30} color={"black"} />{" "}
          <FontAwesome5 name="creative-commons-nc" size={30} color={"black"} />{" "}
          <FontAwesome5 name="creative-commons-nd" size={30} color={"black"} />
          {"\n"}
          {"저작자표시-비영리-변경금지"}
        </Text>
      );
    }
  };
  const getCopyrightDescription = () => {
    if (!byState && !ncState && !saState && !ndState) {
      return "명시적인 허가 없이 다른 사용자들이 내 작품을 사용할 수 없습니다";
    } else if (byState && !ncState && !saState && !ndState) {
      return "저작자 및 출처만 표시한다면,\n제한없이 자유롭게 이용할 수 있습니다.";
    } else if (byState && ncState && !saState && !ndState) {
      return "저작자를 밝히면 복사 및 배포가 가능하지만\n영리목적으로 이용할 수 없습니다.";
    } else if (byState && !ncState && !saState && ndState) {
      return "저작자를 밝히면 복사 및 배포가 가능하지만,\n2차 저작물을 만들어서는 안됩니다.";
    } else if (byState && !ncState && saState && !ndState) {
      return "저작자를 밝히면 복사 및 배포가 가능하고 \n2차적 저작물에 동일한 라이선스를 적용한 경우에만\n저작물의 변경이 가능합니다.";
    } else if (byState && ncState && saState && !ndState) {
      return "저작자를 밝히면 이용이 가능하며 저작물의 변경도 가능하지만,\n 영리목적으로 이용할 수 없고 2차적 저작물에는\n원 저작물과 동일한 라이선스를 적용해야 합니다.";
    } else if (byState && ncState && !saState && ndState) {
      return "저작자를 밝히면 복사 및 배포가 가능하지만,\n영리목적으로 이용할 수 없고 2차 저작물을 만들어서는 안됩니다.";
    }
  };
  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <FloatingButtonContainer>
        <CopyrightHeader>
          <CopyrightHeaderText style={{ fontFamily: "NotoSansKR_700Bold" }}>
            {"필디는 회원님의 작품을 보호합니다!\n저작권 조건을 선택해 주세요."}
          </CopyrightHeaderText>
          <RequiredText style={{ fontFamily: "NotoSansKR_700Bold" }}>
            {"* 저쟉권 권한 설정은 필수입니다."}
          </RequiredText>
        </CopyrightHeader>

        <CopyrightButton>
          <Touchable onPress={() => onClickBY()}>
            <BYButton
              style={{
                backgroundColor: !byState ? "#E2E2E2" : styles.feeeldColor,
              }}
            >
              <BYButtonText
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                  color: !byState ? "grey" : "#fff",
                }}
              >
                {"저작자 표시"}
              </BYButtonText>
            </BYButton>
          </Touchable>
          <Touchable onPress={() => onClickNC()}>
            <NCButton
              style={{
                backgroundColor: !ncState ? "#E2E2E2" : styles.feeeldColor,
              }}
            >
              <NCButtonText
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                  color: !ncState ? "grey" : "#fff",
                }}
              >
                {"비영리"}
              </NCButtonText>
            </NCButton>
          </Touchable>
        </CopyrightButton>

        <CopyrightButton>
          <Touchable onPress={() => onClickSA()}>
            <SAButton
              style={{
                backgroundColor: !saState ? "#E2E2E2" : styles.feeeldColor,
              }}
            >
              <SAButtonText
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                  color: !saState ? "grey" : "#fff",
                }}
              >
                {"동일 조건\n수정 가능"}
              </SAButtonText>
            </SAButton>
          </Touchable>
          <Touchable onPress={() => onClickND()}>
            <NDButton
              style={{
                backgroundColor: !ndState ? "#E2E2E2" : styles.feeeldColor,
              }}
            >
              <NDButtonText
                style={{
                  fontFamily: "NotoSansKR_700Bold",
                  color: !ndState ? "grey" : "#fff",
                }}
              >
                {"모든 조건\n수정 금지"}
              </NDButtonText>
            </NDButton>
          </Touchable>
        </CopyrightButton>

        <CopyrightDetail>
          <Touchable onPress={() => alert("Heart Button!")}>
            <CopyrightDetailText
              style={{ fontFamily: "NotoSansKR_700Bold" }}
              onPress={() => setVisibleModal(true)}
            >
              {"Common Creative License 상세히 보기"}
            </CopyrightDetailText>
          </Touchable>
          <Border></Border>
        </CopyrightDetail>

        <CopyrightTitle style={{ fontFamily: "NotoSansKR_900Black" }}>
          {getCopyrightTitle()}
        </CopyrightTitle>
        <CopyrightDescription style={{ fontFamily: "NotoSansKR_400Regular" }}>
          {getCopyrightDescription()}
        </CopyrightDescription>

        <CopyrightModal visibleModal={visibleModal} closeModal={closeModal} />
      </FloatingButtonContainer>
    );
  }
};

export default Copyright;
