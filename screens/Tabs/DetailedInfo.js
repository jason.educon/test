/**
 * 작품 상세 정보를 입력하는 탭입니다.
 *
 */

import React, { useState } from "react";
import { Text, View, Button } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Moment from "moment";

import styled from "styled-components";
import GlobalStyles from "../../styles";

import {
  FloatingButtonContainer,
  Touchable,
  TagText_FilterModal,
  TagList_FilterModal
} from "../../components/GlobalStyledComponent";

import PeriodPickerModal from "../../components/PeriodPickerModal";
import TagFilterModal from "../../components/TagFilterModal";
import ToolFilterModal from "../../components/ToolFilterModal";

import { Feather } from "@expo/vector-icons";
import styles from "../../styles";

const HeaderContainer = styled.View`
  padding-left: 20px;
  margin-top: 30px;
  margin-bottom: 20px;
`;

const HeaderText = styled.Text`
  font-family: "bold";
  font-size: 20px;
  padding: 0px;
  include-font-padding: false;
  text-align-vertical: center;
`;

const OptionText = styled.Text`
  font-family: "bold";
  font-size: 12px;
  color: #50d6a8;
  margin-left: 5px;
`;

const SubContainer = styled.View`
  margin: 5px 15px;
  padding: 10px 0px;
`;

const SubTitleText = styled.Text`
  font-family: "bold";
  font-size: 15px;
  margin: 0px 0px 5px;
`;

const SubButtonView = styled.View`
  background-color: ${GlobalStyles.lightGrayColor};
  border-radius: 15px;
  align-items: center;
  padding: 10px;
  margin-top: 10px;
`;

const SubButtonText = styled.Text`
  font-family: "regular";
  font-size: 12px;
  padding: 0px;
  include-font-padding: false;
  text-align-vertical: center;
`;

const CategoryContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const CategoryItem = styled.View`
  border-radius: 10px;
  margin: 5px;
  padding: 10px;
`;

const CategoryItemText = styled.Text`
  font-family: "regular";
  font-size: 12px;
  padding: 0px;
  include-font-padding: false;
  text-align-vertical: center;
`;

export default () => {
  const categoryList = ["없음", "건축", "실내", "제품", "웹", "시각"];

  const renderCategoryList = () => {
    const [activeCategory, setActiveCategory] = useState(0);

    const onClickCategory = index => {
      setActiveCategory(index);
    };

    return categoryList.map((category, index) => (
      <Touchable
        key={index}
        active={activeCategory === index}
        onPress={() => onClickCategory(index)}
      >
        <CategoryItem
          style={{
            backgroundColor:
              activeCategory === index
                ? styles.feeeldColor
                : GlobalStyles.lightGrayColor
          }}
        >
          <CategoryItemText>{category}</CategoryItemText>
        </CategoryItem>
      </Touchable>
    ));
  };

  const [visiblePeriodModal, setVisiblePeriodModal] = useState(false);
  const [visibleTagFilterModal, setVisibleTagFilterModal] = useState(false);
  const [visibleToolFilterModal, setVisibleToolFilterModal] = useState(false);

  const closePeriodPickerModal = () => {
    setVisiblePeriodModal(false);
  };

  const closeTagFilterModal = () => {
    setVisibleTagFilterModal(false);
  };
  const closeToolFilterModal = () => {
    setVisibleToolFilterModal(false);
  };

  const [selectedStartDate, setSelectedStartDate] = useState(null);
  const [selectedEndDate, setSelectedEndDate] = useState(null);
  const [selectedTags, setSelectedTags] = useState([]);
  const [selectedTools, setSelectedTools] = useState([]);

  const startDate = selectedStartDate
    ? Moment(selectedStartDate)
        .format("YYYY.MM.DD")
        .toString()
    : "";
  const endDate = selectedEndDate
    ? Moment(selectedEndDate)
        .format("YYYY.MM.DD")
        .toString()
    : "";

  return (
    <FloatingButtonContainer>
      <KeyboardAwareScrollView
        scrollEventThrottle={16}
        showsVerticalScrollIndicator={false}
      >
        <HeaderContainer>
          <HeaderText>
            필디 회원들은 궁금합니다!{"\n"}작품을 기록으로 남겨주세요.
          </HeaderText>
          <OptionText>* 작품 상세 정보 입력은 자유입니다</OptionText>
        </HeaderContainer>
        <SubContainer>
          <SubTitleText>프로젝트 분류</SubTitleText>
          <CategoryContainer>{renderCategoryList()}</CategoryContainer>
        </SubContainer>
        <SubContainer>
          <SubTitleText>프로젝트 기간</SubTitleText>
          <Touchable onPress={() => setVisiblePeriodModal(true)}>
            <SubButtonView>
              {selectedStartDate == null && selectedEndDate == null ? (
                <SubButtonText>기간을 선택해 주세요</SubButtonText>
              ) : (
                <SubButtonText>
                  {startDate}~{endDate}
                </SubButtonText>
              )}
            </SubButtonView>
          </Touchable>
        </SubContainer>
        <SubContainer>
          <SubTitleText>사용 프로그램</SubTitleText>
          <Touchable onPress={() => setVisibleToolFilterModal(true)}>
            {selectedTools.length == 0 ? (
              <SubButtonView>
                <SubButtonText>프로그램을 선택해 주세요</SubButtonText>
              </SubButtonView>
            ) : (
              <TagList_FilterModal>
                {selectedTools.map((tool, index) => (
                  <TagText_FilterModal key={index}>{tool}</TagText_FilterModal>
                ))}
              </TagList_FilterModal>
            )}
          </Touchable>
        </SubContainer>
        <SubContainer>
          <SubTitleText>프로젝트 태그</SubTitleText>
          <Touchable onPress={() => setVisibleTagFilterModal(true)}>
            {selectedTags.length == 0 ? (
              <SubButtonView>
                <SubButtonText>태그를 선택해 주세요</SubButtonText>
              </SubButtonView>
            ) : (
              <TagList_FilterModal>
                {selectedTags.map((tag, index) => (
                  <TagText_FilterModal key={index}>
                    {tag + " "}
                  </TagText_FilterModal>
                ))}
              </TagList_FilterModal>
            )}
          </Touchable>
        </SubContainer>
      </KeyboardAwareScrollView>
      <PeriodPickerModal
        visibleModal={visiblePeriodModal}
        closeModal={closePeriodPickerModal}
        selectedStartDate={selectedStartDate}
        selectedEndDate={selectedEndDate}
        setSelectedStartDate={setSelectedStartDate}
        setSelectedEndDate={setSelectedEndDate}
      />
      <TagFilterModal
        visibleModal={visibleTagFilterModal}
        closeModal={closeTagFilterModal}
        selectedTags={selectedTags}
        setSelectedTags={setSelectedTags}
      />
      <ToolFilterModal
        visibleModal={visibleToolFilterModal}
        closeModal={closeToolFilterModal}
        selectedTools={selectedTools}
        setSelectedTools={setSelectedTools}
      />
    </FloatingButtonContainer>
  );
};
