/**
 * 로컬 로그인 스크린
 * 로그인 성공시 jwtToken을 얻는다.
 */
import React, { useState } from "react";
import {
  View,
  Switch,
  Text,
  Dimensions,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
} from "react-native";
import { AppLoading } from "expo";
import Constants from "expo-constants";
import { useMutation } from "react-apollo-hooks";

import styled from "styled-components";
import GlobalStyles from "../../styles";
import { AntDesign } from "@expo/vector-icons";

import useInput from "../../hooks/useInput";
import constants from "../../constants";
import AuthInput from "../../components/AuthInput";
import AuthButton from "../../components/AuthButton";
import { LOG_IN } from "./Queries";
import { useLogIn } from "../../AuthContext";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";
import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const Touchable = styled.TouchableOpacity``;

const UserContainer = styled.SafeAreaView`
  background-color: white;
  flex: 1;
  justify-content: flex-start;
  padding-top: ${constants.statusBarHeight}px;
`;

const CloseContainer = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  margin-top: 15px;
  margin-right: 15px;
`;

const UserHeader = styled.View`
  height: ${constants.height / 5}px;
  align-items: flex-start;
  justify-content: center;
  padding-left: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
`;

const UserHeaderLogo = styled.Image`
  width: ${constants.width / 6}px;
  height: ${constants.width / 6}px;
`;

const UserHeaderText = styled.Text`
  ${GlobalStyles.h1};
  padding-top: 10px;
`;

const LoginContainer = styled.View`
  justify-content: flex-start;
  align-items: center;
  margin-top: 10px;
`;

const RowContainer = styled.View`
  width: ${constants.width / 1.1}px;
  padding: 5px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const FindLink = styled.Text`
  ${GlobalStyles.h4};
  text-align: left;
  font-weight: 600;
  text-decoration: underline;
  color: #707070;
`;

const AutoLogin = styled.View`
  flex-direction: row;
  align-items: center;
  text-align: right;
`;

const AutoLoginText = styled.Text`
  ${GlobalStyles.h4};
  padding-right: 5px;
`;

const JoinContainer = styled.View`
  padding-top: 20px;
  justify-content: flex-start;
  align-items: center;
`;

const JoinTextView = styled.View`
  flex-direction: row;
  align-items: center;
`;

const JoinText = styled.Text`
  ${GlobalStyles.h4};
`;

const LinkText = styled.Text`
  ${GlobalStyles.h4};
  color: ${GlobalStyles.feeeldColor};
  font-weight: 600;
  text-align: center;
  padding-left: 10px;
  text-decoration: underline ${GlobalStyles.feeeldColor};
`;

const verifyEmail = (email) => {
  const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  let hasError = false;
  if (email === "") {
    Alert.alert("Email can't be empty");
    hasError = true;
  } else if (!email.includes("@") || !email.includes(".")) {
    Alert.alert("Please write an email");
    hasError = true;
  } else if (!emailRegex.test(email)) {
    Alert.alert("That email is invalid");
    hasError = true;
  }
  return !hasError;
};

const verifyPassword = (password) => {
  let hasError = false;
  if (password.trim() === "") {
    Alert.alert("password can't be empty");
    hasError = true;
  }
  return !hasError;
};

export default ({ route, navigation }) => {
  setNavHeader({ navigation, visible: false });

  const login = useLogIn();

  const emailInput = useInput("");
  const pwInput = useInput("");

  const [autoLogin, setAutoLogin] = useState(true);
  const toggleAutoLogin = () => {
    setAutoLogin(!autoLogin);
  };

  const [loading, setLoading] = useState(false);

  const [confirmLocalLogin] = useMutation(LOG_IN);

  const handleLogin = async () => {
    if (!verifyEmail(emailInput.value)) {
      return;
    }

    if (!verifyPassword(pwInput.value)) {
      return;
    }

    try {
      setLoading(true);

      const { data } = await confirmLocalLogin({
        variables: {
          email: emailInput.value,
          password: pwInput.value,
        },
      });
      const { confirmLocalLogin: jwtToken } = data;

      if (jwtToken != "INVALID_PAIR") {
        Alert.alert("로그인 성공, 토큰: " + jwtToken);
        login(jwtToken);
        navigation.popToTop();
        return;
      } else {
        Alert.alert("아이디 혹은 비밀번호가 잘못 됐습니다.");
        return;
      }
    } catch (e) {
      console.log(e);
      Alert.alert("서버에 오류가 발생했습니다.");
    } finally {
      setLoading(false);
    }
  };

  return (
    <FloatingButtonContainer>
      <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        style={{ flex: 1 }}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <UserContainer>
            <UserHeader>
              <UserHeaderLogo
                source={require("../../assets/feeeld_logo.png")}
              />
              <UserHeaderText>필디에 로그인하기</UserHeaderText>
            </UserHeader>
            <LoginContainer>
              <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View>
                  <AuthInput
                    {...emailInput}
                    placeholder="이메일"
                    keyboardType="email-address"
                    textContentType="emailAddress"
                    returnKeyType="send"
                    onSubmitEditing={handleLogin}
                    autoCorrect={false}
                    secureTextEntry={false}
                  />
                  <AuthInput
                    {...pwInput}
                    placeholder="비밀번호"
                    keyboardType="default"
                    textContentType="password"
                    returnKeyType="send"
                    autoCorrect={false}
                    secureTextEntry={true}
                  />
                </View>
              </TouchableWithoutFeedback>
              <RowContainer>
                <Touchable>
                  <FindLink>이메일/비밀번호 찾기</FindLink>
                </Touchable>
                <AutoLogin>
                  <AutoLoginText>자동 로그인</AutoLoginText>
                  <Switch onValueChange={toggleAutoLogin} value={autoLogin} />
                </AutoLogin>
              </RowContainer>
              <AuthButton
                loading={loading}
                onPress={handleLogin}
                bgColor={GlobalStyles.grayColor}
                text="로그인"
              />
            </LoginContainer>
            <JoinContainer>
              <JoinTextView>
                <JoinText>필디 계정이 없으신가요?</JoinText>
                <Touchable onPress={() => navigation.navigate("PhoneAuth")}>
                  <LinkText>회원가입</LinkText>
                </Touchable>
              </JoinTextView>
            </JoinContainer>
          </UserContainer>
        </TouchableWithoutFeedback>
        <FloatingButton
          icon="left"
          onPress={() => navigation.goBack()}
          direction="left"
          visible={true}
        />
      </KeyboardAvoidingView>
    </FloatingButtonContainer>
  );
};
