/**
 * 필수 정보 기입 스크린
 * 입력 완료시 ConfirmPolicy.js 로 넘어간다.
 * [!] 비밀번호 확인 기능
 */
import React, { useState } from "react";
import {
  View,
  Switch,
  Text,
  Dimensions,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView
} from "react-native";
import { AppLoading } from "expo";
import Constants from "expo-constants";
import { useMutation } from "react-apollo-hooks";
import IntlPhoneInput from "react-native-intl-phone-input";

import styled from "styled-components";
import styles from "../../styles";
import { AntDesign } from "@expo/vector-icons";

import useInput from "../../hooks/useInput";
import constants from "../../constants";
import AuthInput from "../../components/AuthInput";
import AuthButton from "../../components/AuthButton";

import {
  useFonts,
  NotoSansKR_100Thin,
  NotoSansKR_300Light,
  NotoSansKR_400Regular,
  NotoSansKR_500Medium,
  NotoSansKR_700Bold,
  NotoSansKR_900Black
} from "@expo-google-fonts/noto-sans-kr";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";
import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const Touchable = styled.TouchableOpacity``;

const MainContainer = styled.SafeAreaView`
  background-color: white;
  flex: 1;
  justify-content: flex-start;

  padding-top: ${constants.statusBarHeight}px;
`;

const UserHeader = styled.View`
  height: ${constants.height / 5}px;
  align-items: flex-start;
  justify-content: center;
  padding-left: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
`;

const UserHeaderLogo = styled.Image`
  width: ${constants.width / 6}px;
  height: ${constants.width / 6}px;
`;

const UserHeaderText = styled.Text`
  font-size: 24px;
  padding-top: 10px;
`;

const AuthContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`;

const verifyEmail = email => {
  const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  let hasError = false;
  if (email === "") {
    Alert.alert("Email can't be empty");
    hasError = true;
  } else if (!email.includes("@") || !email.includes(".")) {
    Alert.alert("Please write an email");
    hasError = true;
  } else if (!emailRegex.test(email)) {
    Alert.alert("That email is invalid");
    hasError = true;
  }
  return !hasError;
};

const verifyPassword = password => {
  let hasError = false;
  if (password.trim() === "") {
    Alert.alert("password can't be empty");
    hasError = true;
  }
  return !hasError;
};

export default ({ route, navigation }) => {
  setNavHeader({ navigation, visible: false });

  const { phoneNumber, nickname, email, password } = route.params;

  let [fontsLoaded] = useFonts({
    NotoSansKR_400Regular,
    NotoSansKR_700Bold
  });

  const [loading, setLoading] = useState(false);

  const nickInput = useInput(nickname);
  const emailInput = useInput(email);
  const pwInput = useInput(password);

  const handleInput = async () => {
    if (!verifyEmail(emailInput.value)) {
      return;
    }
    if (!verifyPassword(pwInput.value)) {
      return;
    }
  };

  const submit = async () => {
    navigation.navigate("ConfirmPolicy", {
      phone: phoneNumber.trim(),
      nickname: nickInput.value.trim(),
      email: emailInput.value.trim(),
      password: pwInput.value.trim()
    });
  };

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <FloatingButtonContainer>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          style={{ flex: 1 }}
        >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <MainContainer>
              <UserHeader>
                <UserHeaderLogo
                  source={require("../../assets/feeeld_logo.png")}
                />
                <UserHeaderText style={{ fontFamily: "NotoSansKR_700Bold" }}>
                  필디에 오신 것을 환영합니다.
                </UserHeaderText>
              </UserHeader>
              <AuthContainer>
                <AuthInput
                  {...nickInput}
                  placeholder="닉네임 (feeel;명)"
                  keyboardType="default"
                  textContentType="nickname"
                  returnKeyType="send"
                  onSubmitEditing={handleInput}
                  autoCorrect={false}
                  secureTextEntry={false}
                />
                <AuthInput
                  {...emailInput}
                  placeholder="이메일"
                  keyboardType="email-address"
                  textContentType="emailAddress"
                  returnKeyType="send"
                  onSubmitEditing={handleInput}
                  autoCorrect={false}
                  secureTextEntry={false}
                />
                <AuthInput
                  {...pwInput}
                  placeholder="비밀번호"
                  keyboardType="default"
                  textContentType="password"
                  returnKeyType="send"
                  autoCorrect={false}
                  secureTextEntry={true}
                />

                <AuthButton
                  loading={loading}
                  onPress={() => submit()}
                  bgColor={styles.grayColor}
                  text="계속"
                />
              </AuthContainer>
            </MainContainer>
          </TouchableWithoutFeedback>
          <FloatingButton
            icon="left"
            onPress={() => navigation.goBack()}
            direction="left"
            visible={true}
          />
          <FloatingButton
            icon="right"
            onPress={() => alert("다음 버튼 (연결 x)")}
            direction="right"
            visible={true}
          />
        </KeyboardAvoidingView>
      </FloatingButtonContainer>
    );
  }
};
