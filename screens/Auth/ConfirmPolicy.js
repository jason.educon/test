/**
 * 기본정보 입력 후 이용 약관 등에 동의하는 페이지,
 * 이전 페이지로 부터 회원 입력 정보를 받고 최종 동의 시 가입 요청을 보낸다.
 * [!] 이전 페이지도 동일하게 적용되는 사항 ; 뒤로 가기시 폼이 채워져있는가?
 */
import React, { useState } from "react";
import { ScrollView, Alert } from "react-native";
import { AppLoading } from "expo";
import Constants from "expo-constants";
import { useMutation } from "react-apollo-hooks";
import IntlPhoneInput from "react-native-intl-phone-input";

import styled from "styled-components";
import GlobalStyles from "../../styles";
import { AntDesign } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";

import useInput from "../../hooks/useInput";
import constants from "../../constants";
import AuthInput from "../../components/AuthInput";
import AuthButton from "../../components/AuthButton";
import { CREATE_ACCOUNT } from "./Queries";

import {
  useFonts,
  NotoSansKR_100Thin,
  NotoSansKR_300Light,
  NotoSansKR_400Regular,
  NotoSansKR_500Medium,
  NotoSansKR_700Bold,
  NotoSansKR_900Black
} from "@expo-google-fonts/noto-sans-kr";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";
import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const Touchable = styled.TouchableOpacity``;

const MainContainer = styled.SafeAreaView`
  background-color: white;
  flex: 1;
  justify-content: flex-start;

  padding-top: ${constants.statusBarHeight}px;
`;

const CloseContainer = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  margin-top: 15px;
  margin-right: 15px;
`;

const UserHeader = styled.View`
  height: ${constants.height / 5}px;
  align-items: flex-start;
  justify-content: center;
  padding-left: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
`;

const UserHeaderLogo = styled.Image`
  width: ${constants.width / 6}px;
  height: ${constants.width / 6}px;
`;

const UserHeaderText = styled.Text`
  font-size: 24px;
  padding-top: 10px;
`;

const ConfirmContainer = styled.View`
  justify-content: flex-start;
  align-items: flex-start;
  margin-top: 10px;
  padding: 20px;
`;

const ConfirmTitle = styled.Text`
  font-size: 18px;
  padding-bottom: 10px;
`;

const ConfirmList = styled.View`
  margin: 5px;
`;

const ConfirmItem = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding-bottom: 20px;
`;

const ConfirmItemIcon = styled.View`
  align-items: center;
  justify-content: center;
  width: 35px;
`;

const ConfirmItemText = styled.View`
  margin-left: 5px;
  padding-left: 5px;
  align-items: flex-start;
`;

const ConfirmAll = styled.View`
  justify-content: center;
  align-items: center;
`;

const ConfirmText = styled.Text`
  font-size: 12px;
  color: ${GlobalStyles.blackColor};
`;

const ConfirmFocusText = styled.Text`
  font-size: 12px;
  text-decoration: underline #5cf3a9;
  color: ${GlobalStyles.feeeldColor};
  padding-top: ${constants.specialPadding}px;
  margin-top: ${constants.specialPadding}px;
`;

const ButtonContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
`;

const ButtonText = styled.Text`
  color: white;
  font-size: 20px;
`;

const ConfirmButton = styled.View`
  ${GlobalStyles.feeeldBox};
  background-color: ${GlobalStyles.feeeldColor};
  width: ${constants.width / 2.5}px;
  align-items: center;
  padding: 5px;
`;

const RejectButton = styled.View`
  ${GlobalStyles.feeeldBox};
  border-color: ${GlobalStyles.grayColor};
  background-color: ${GlobalStyles.grayColor};
  width: ${constants.width / 2.5}px;
  align-items: center;
  padding: 5px;
`;

export default ({ route, navigation }) => {
  setNavHeader({ navigation, visible: false });
  const { phone, email, password, nickname: _nickname } = route.params;

  const [createAccountMutation] = useMutation(CREATE_ACCOUNT);

  let [fontsLoaded] = useFonts({
    NotoSansKR_400Regular,
    NotoSansKR_700Bold
  });

  const [loading, setLoading] = useState(false);

  const submit = async () => {
    const { data } = await createAccountMutation({
      variables: {
        phone,
        nickname: _nickname, // 아래 nickname과 충돌해서 null로 들어가는 현상
        email,
        password
      }
    });
    const {
      createAccount: { nickname }
    } = data;

    Alert.alert(`'${nickname}'으로 회원가입 완료.`);
    navigation.navigate("LocalLogin");
  };

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <MainContainer>
        <ScrollView>
          <UserHeader>
            <UserHeaderLogo source={require("../../assets/feeeld_logo.png")} />
            <UserHeaderText style={{ fontFamily: "NotoSansKR_700Bold" }}>
              필디는 회원님의 작품을 보호합니다.
            </UserHeaderText>
          </UserHeader>
          <ConfirmContainer>
            <ConfirmTitle style={{ fontFamily: "NotoSansKR_700Bold" }}>
              이용 약관 및 개인정보정책 요약
            </ConfirmTitle>
            <ConfirmList>
              <ConfirmItem>
                <ConfirmItemIcon>
                  <FontAwesome5
                    name="user-lock"
                    size={26}
                    color={GlobalStyles.blackColor}
                  />
                </ConfirmItemIcon>
                <ConfirmItemText>
                  <ConfirmText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                    회원님이 서비스 내에 게시한 게시물은
                  </ConfirmText>
                  <ConfirmFocusText
                    style={{ fontFamily: "NotoSansKR_400Regular" }}
                  >
                    저작권법에 의해 보호를 받습니다.
                  </ConfirmFocusText>
                </ConfirmItemText>
              </ConfirmItem>
              <ConfirmItem>
                <ConfirmItemIcon>
                  <FontAwesome5
                    name="database"
                    size={26}
                    color={GlobalStyles.blackColor}
                  />
                </ConfirmItemIcon>
                <ConfirmItemText>
                  <ConfirmText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                    회원가입시 계정과 회원정보를 수집합니다.
                  </ConfirmText>
                  <ConfirmFocusText
                    style={{ fontFamily: "NotoSansKR_400Regular" }}
                  >
                    정보들은 서비스 제공을 위해 활용됩니다.
                  </ConfirmFocusText>
                </ConfirmItemText>
              </ConfirmItem>
              <ConfirmItem>
                <ConfirmItemIcon>
                  <FontAwesome5
                    name="user-lock"
                    size={26}
                    color={GlobalStyles.blackColor}
                  />
                </ConfirmItemIcon>
                <ConfirmItemText>
                  <ConfirmText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                    회원님이 서비스 내에 게시한 게시물은
                  </ConfirmText>
                  <ConfirmFocusText
                    style={{ fontFamily: "NotoSansKR_400Regular" }}
                  >
                    저작권법에 의해 보호를 받습니다.
                  </ConfirmFocusText>
                </ConfirmItemText>
              </ConfirmItem>
              <ConfirmItem>
                <ConfirmItemIcon>
                  <FontAwesome5
                    name="database"
                    size={26}
                    color={GlobalStyles.blackColor}
                  />
                </ConfirmItemIcon>
                <ConfirmItemText>
                  <ConfirmText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                    회원가입시 계정과 회원정보를 수집합니다.
                  </ConfirmText>
                  <ConfirmFocusText
                    style={{ fontFamily: "NotoSansKR_400Regular" }}
                  >
                    정보들은 서비스 제공을 위해 활용됩니다.
                  </ConfirmFocusText>
                </ConfirmItemText>
              </ConfirmItem>
            </ConfirmList>
          </ConfirmContainer>
          <ConfirmAll>
            <Touchable>
              <ConfirmFocusText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                전문보기
              </ConfirmFocusText>
            </Touchable>
          </ConfirmAll>
          <ButtonContainer>
            <Touchable onPress={() => navigation.navigate("LocalLogin")}>
              <RejectButton>
                <ButtonText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                  거부
                </ButtonText>
              </RejectButton>
            </Touchable>
            <Touchable onPress={() => submit()}>
              <ConfirmButton>
                <ButtonText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                  동의
                </ButtonText>
              </ConfirmButton>
            </Touchable>
          </ButtonContainer>
        </ScrollView>
      </MainContainer>
    );
  }
};
