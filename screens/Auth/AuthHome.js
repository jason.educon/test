/**
 * 인증 첫화면을 구성한다.
 *
 * 이메일 계정으로 시작하기 클릭시, 로컬 로그인 화면으로 라우팅 된다.
 * 그 외 sns 로그인 클릭시, sns 인증을 시도한다.
 *
 * [!] google auth 구현
 * [!] sns 로그인 클릭 시 회원가입 연계
 */
import React, { useState } from "react";
import { Dimensions, Alert, View } from "react-native";
import { AppLoading } from "expo";

import styled from "styled-components";
import GlobalStyles from "../../styles";
import { AntDesign } from "@expo/vector-icons";

import constants from "../../constants";

import {
  useFonts,
  NotoSansKR_100Thin,
  NotoSansKR_300Light,
  NotoSansKR_400Regular,
  NotoSansKR_500Medium,
  NotoSansKR_700Bold,
  NotoSansKR_900Black
} from "@expo-google-fonts/noto-sans-kr";

import * as Facebook from "expo-facebook";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";
import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const UserContainer = styled.SafeAreaView`
  background-color: white;
  flex: 1;
  justify-content: flex-start;
  padding-top: ${constants.statusBarHeight}px;
`;

const EmptyContainer = styled.View`
  height: ${constants.height / 6}px;
`;

const CloseContainer = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  margin-top: 15px;
  margin-right: 15px;
`;

const UserHeader = styled.View`
  height: ${constants.height / 5}px;
  align-items: flex-start;
  justify-content: center;
  padding-left: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
`;

const UserHeaderLogo = styled.Image`
  width: ${constants.width / 6}px;
  height: ${constants.width / 6}px;
`;

const UserHeaderText = styled.Text`
  font-size: 24px;
  padding-top: 10px;
`;

const Touchable = styled.TouchableOpacity``;

const LoginContainer = styled.View`
  justify-content: center;
  align-items: center;
`;

const LoginBox = styled.View`
  ${GlobalStyles.feeeldBox};
  border-color: ${GlobalStyles.grayColor};
  border-radius: 15px;
  padding: 3px;
  margin: 5px;
  width: ${constants.width - 40}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const LoginText = styled.Text`
  max-width: ${constants.width - 80}px;
  min-width: 168px;
  font-size: 12px;
  text-align: center;
  padding-left: 5px;
`;

const SmallLogo = styled.Image`
  margin-left: 5px;
  width: 24px;
  height: 24px;
`;

const facebookLogIn = async () => {
  try {
    await Facebook.initializeAsync(); // '<APP_ID>'
    const {
      type,
      token,
      expires,
      permissions,
      declinedPermissions
    } = await Facebook.logInWithReadPermissionsAsync({
      permissions: ["public_profile"]
    });
    if (type === "success") {
      // Get the user's name using Facebook's Graph API
      const response = await fetch(
        `https://graph.facebook.com/me?access_token=${token}`
      );
      Alert.alert("Logged in!", `Hi ${(await response.json()).name}!`);
    } else {
      // type === 'cancel'
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
  }
};

export default ({ navigation }) => {
  setNavHeader({ navigation, visible: false });

  let [fontsLoaded] = useFonts({
    NotoSansKR_400Regular,
    NotoSansKR_700Bold
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <FloatingButtonContainer>
        <UserContainer>
          <UserHeader>
            <UserHeaderLogo source={require("../../assets/feeeld_logo.png")} />
            <UserHeaderText style={{ fontFamily: "NotoSansKR_700Bold" }}>
              디자인을 세상과 연결, 필디
            </UserHeaderText>
          </UserHeader>
          <EmptyContainer></EmptyContainer>
          <LoginContainer>
            <Touchable onPress={() => navigation.navigate("LocalLogin")}>
              <LoginBox>
                <SmallLogo source={require("../../assets/feeeld_logo.png")} />
                <LoginText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                  이메일 계정으로 시작하기
                </LoginText>
              </LoginBox>
            </Touchable>
            <Touchable>
              <LoginBox>
                <SmallLogo source={require("../../assets/kakao-talk.png")} />
                <LoginText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                  카카오 계정으로 시작하기
                </LoginText>
              </LoginBox>
            </Touchable>
            <Touchable>
              <LoginBox>
                <SmallLogo source={require("../../assets/naver.png")} />
                <LoginText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                  네이버 계정으로 시작하기
                </LoginText>
              </LoginBox>
            </Touchable>
            <Touchable>
              <LoginBox>
                <SmallLogo source={require("../../assets/google.png")} />
                <LoginText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                  구글 계정으로 시작하기
                </LoginText>
              </LoginBox>
            </Touchable>
            <Touchable onPress={() => facebookLogIn()}>
              <LoginBox>
                <SmallLogo source={require("../../assets/facebook.png")} />
                <LoginText style={{ fontFamily: "NotoSansKR_400Regular" }}>
                  페이스북 계정으로 시작하기
                </LoginText>
              </LoginBox>
            </Touchable>
          </LoginContainer>
        </UserContainer>
        <FloatingButton
          icon="left"
          onPress={() => navigation.goBack()}
          direction="left"
          visible={true}
        />
      </FloatingButtonContainer>
    );
  }
};
