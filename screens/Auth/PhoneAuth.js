/**
 * 로컬 회원가입 시작 스크린으로
 * 휴대폰 번호를 입력 받는다.
 * 지금은 문자를 시뮬레이션 하기 위해 얼러트로 코드를 띄워준다.
 * [!] 국가 번호 처리에 대한 고민
 * [!] 실제 문자를 보낼 때는, 폰 번호가 존재하지 않는 것 처리
 */
import React, { useState } from "react";
import {
  View,
  Switch,
  Text,
  Dimensions,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView
} from "react-native";
import { AppLoading } from "expo";
import Constants from "expo-constants";
import { useMutation } from "react-apollo-hooks";
import IntlPhoneInput from "react-native-intl-phone-input";

import styled from "styled-components";
import styles from "../../styles";
import { AntDesign } from "@expo/vector-icons";

import useInput from "../../hooks/useInput";
import constants from "../../constants";
import AuthInput from "../../components/AuthInput";
import AuthButton from "../../components/AuthButton";
import { AUTH_PHONE } from "./Queries";

import {
  useFonts,
  NotoSansKR_100Thin,
  NotoSansKR_300Light,
  NotoSansKR_400Regular,
  NotoSansKR_500Medium,
  NotoSansKR_700Bold,
  NotoSansKR_900Black
} from "@expo-google-fonts/noto-sans-kr";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";
import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const Touchable = styled.TouchableOpacity``;

const MainContainer = styled.SafeAreaView`
  background-color: white;
  flex: 1;
  justify-content: flex-start;

  padding-top: ${constants.statusBarHeight}px;
`;

const CloseContainer = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  margin-top: 15px;
  margin-right: 15px;
`;

const UserHeader = styled.View`
  height: ${constants.height / 5}px;
  align-items: flex-start;
  justify-content: center;
  padding-left: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
`;

const UserHeaderLogo = styled.Image`
  width: ${constants.width / 6}px;
  height: ${constants.width / 6}px;
`;

const UserHeaderText = styled.Text`
  font-size: 24px;
  padding-top: 10px;
`;

const AuthContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`;

export default ({ navigation }) => {
  setNavHeader({ navigation, visible: false });

  let [fontsLoaded] = useFonts({
    NotoSansKR_400Regular,
    NotoSansKR_700Bold
  });

  const [phoneNumber, setPhoneNumber] = useState("");
  const [loading, setLoading] = useState(false);

  const [authPhoneMutation] = useMutation(AUTH_PHONE);

  const onChangeText = ({
    dialCode,
    unmaskedPhoneNumber,
    phoneNumber,
    isVerified
  }) => {
    setPhoneNumber(`${dialCode} ${phoneNumber}`);
  };

  const authPhone = async () => {
    if (phoneNumber.split()[0].length < 11) {
      Alert.alert("휴대폰 번호 11자리를 입력해 주세요.");
      return;
    }
    setLoading(true);
    const { data } = await authPhoneMutation({
      variables: {
        phone: phoneNumber
      }
    });
    const { authPhone: code } = data;
    setLoading(false);
    // [!] 추후에 이 부분을 제거하고, authPhone 뮤테이션의 리턴 값을 boolean으로 변경
    Alert.alert(`(문자 시뮬레이션) 다음 코드를 입력해주세요. ${code}`);

    navigation.navigate("CodeAuth", { phoneNumber });
  };

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <FloatingButtonContainer>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          style={{ flex: 1 }}
        >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <MainContainer>
              <UserHeader>
                <UserHeaderLogo
                  source={require("../../assets/feeeld_logo.png")}
                />
                <UserHeaderText style={{ fontFamily: "NotoSansKR_700Bold" }}>
                  필디에 오신 것을 환영합니다.
                </UserHeaderText>
              </UserHeader>
              <AuthContainer>
                <IntlPhoneInput
                  onChangeText={onChangeText}
                  defaultCountry="KR"
                  closeText="닫기"
                  modalContainer={{
                    backgroundColor: "red"
                  }}
                  closeButtonStyle={{
                    marginBottom: 300
                  }}
                  modalContainer={{
                    margin: 5
                  }}
                  phoneInputStyle={{
                    fontSize: 25,
                    backgroundColor: "#fff",
                    textAlign: "center",
                    borderRadius: 5,
                    height: 25
                  }}
                  containerStyle={{
                    marginLeft: 20,
                    marginRight: 20
                  }}
                />
                <AuthButton
                  loading={loading}
                  onPress={() => authPhone()}
                  bgColor={styles.grayColor}
                  text="휴대폰 인증하기"
                />
              </AuthContainer>
            </MainContainer>
          </TouchableWithoutFeedback>
          <FloatingButton
            icon="left"
            onPress={() => navigation.goBack()}
            direction="left"
            visible={true}
          />
          <FloatingButton
            icon="right"
            onPress={() => alert("다음 버튼 (연결 x)")}
            direction="right"
            visible={true}
          />
        </KeyboardAvoidingView>
      </FloatingButtonContainer>
    );
  }
};
