/**
 * 회원 가입관련 스크린에서 호출되는 그래프큐엘 쿼리 모음.
 */
import { gql } from "apollo-boost";

export const LOG_IN = gql`
  mutation confirmLocalLogin($email: String!, $password: String!) {
    confirmLocalLogin(email: $email, password: $password)
  }
`;

export const AUTH_PHONE = gql`
  mutation authPhone($phone: String!) {
    authPhone(phone: $phone)
  }
`;

export const AUTH_CODE = gql`
  mutation authCode($phone: String!, $code: String!) {
    authCode(phone: $phone, code: $code)
  }
`;

export const CREATE_ACCOUNT = gql`
  mutation createAccount(
    $phone: String!
    $nickname: String!
    $email: String!
    $password: String!
  ) {
    createAccount(
      phone: $phone
      nickname: $nickname
      email: $email
      password: $password
    ) {
      id
      nickname
    }
  }
`;

export const CONFIRM_SECRET = gql`
  mutation confirmSecret($secret: String!, $email: String!) {
    confirmSecret(secret: $secret, email: $email)
  }
`;
