/**
 * 휴대폰 번호를 입력한 뒤에 코드를 전송 받은 상태에서 코드를 입력할 수 있는 스크린
 * [!] 인증하기 눌렀을 때, 로딩 표시를 해줄 필요가 있다.
 * [!] 진입하자마자 인풋 포커스 필요.
 * [!] 여기서 이미 있는 폰 번호 처리 해주는게 적당
 */
import React, { useState } from "react";
import {
  View,
  Switch,
  Text,
  Dimensions,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet
} from "react-native";
import { AppLoading } from "expo";
import { useMutation } from "react-apollo-hooks";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell
} from "react-native-confirmation-code-field";

import styled from "styled-components";
import styles from "../../styles";
import { AntDesign } from "@expo/vector-icons";

import constants from "../../constants";
import AuthButton from "../../components/AuthButton";
import { AUTH_CODE } from "./Queries";

import {
  useFonts,
  NotoSansKR_100Thin,
  NotoSansKR_300Light,
  NotoSansKR_400Regular,
  NotoSansKR_500Medium,
  NotoSansKR_700Bold,
  NotoSansKR_900Black
} from "@expo-google-fonts/noto-sans-kr";

import { setNavHeader } from "../../utils/navHeader";
import FloatingButton from "../../components/FloatingButton";
import { FloatingButtonContainer } from "../../components/GlobalStyledComponent";

const Touchable = styled.TouchableOpacity``;

const MainContainer = styled.SafeAreaView`
  background-color: white;
  flex: 1;
  justify-content: flex-start;
  padding-top: ${constants.statusBarHeight}px;
`;

const UserHeader = styled.View`
  height: ${constants.height / 5}px;
  align-items: flex-start;
  justify-content: center;
  padding-left: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
`;

const UserHeaderLogo = styled.Image`
  width: ${constants.width / 6}px;
  height: ${constants.width / 6}px;
`;

const UserHeaderText = styled.Text`
  font-size: 24px;
  padding-top: 10px;
`;

const AuthContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`;

const CELL_COUNT = 4;

export default ({ route, navigation }) => {
  setNavHeader({ navigation, visible: false });
  const { phoneNumber } = route.params;

  let [fontsLoaded] = useFonts({
    NotoSansKR_400Regular,
    NotoSansKR_700Bold
  });

  const [code, setCode] = useState("");
  const [authCodeMutation] = useMutation(AUTH_CODE);

  const [loading, setLoading] = useState(false);
  const ref = useBlurOnFulfill({ code, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    code,
    setCode
  });

  const inStyles = StyleSheet.create({
    root: { padding: 20 },
    codeFieldRoot: { marginTop: 5 },
    cell: {
      width: 50,
      height: 50,
      lineHeight: 50,
      fontSize: 24,
      borderWidth: 2,
      borderColor: styles.grayColor,
      borderRadius: 10,
      textAlign: "center",
      margin: 5
    },
    focusCell: {
      borderColor: "#000"
    }
  });

  const authCode = async () => {
    if (!code || code.length < 4) {
      Alert.alert("코드 4 자리를 입력해주세요.");
    }
    setLoading(true);
    const { data } = await authCodeMutation({
      variables: {
        phone: phoneNumber,
        code: code
      }
    });
    const { authCode: success } = data;
    setLoading(false);
    if (success) {
      Alert.alert("코드 일치");
      navigation.navigate("JoinBasic", { phoneNumber });
    } else {
      Alert.alert("코드 불일치");
      setCode("");
    }
  };

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <FloatingButtonContainer>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          style={{ flex: 1 }}
        >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <MainContainer>
              <UserHeader>
                <UserHeaderLogo
                  source={require("../../assets/feeeld_logo.png")}
                />
                <UserHeaderText style={{ fontFamily: "NotoSansKR_700Bold" }}>
                  필디에 오신 것을 환영합니다.
                </UserHeaderText>
              </UserHeader>
              <AuthContainer>
                <CodeField
                  ref={ref}
                  value={code}
                  onChangeText={setCode}
                  cellCount={CELL_COUNT}
                  rootStyle={inStyles.codeFieldRoot}
                  keyboardType="number-pad"
                  textContentType="oneTimeCode"
                  renderCell={({ index, symbol, isFocused }) => (
                    <Text
                      key={index}
                      style={[inStyles.cell, isFocused && inStyles.focusCell]}
                      onLayout={getCellOnLayoutHandler(index)}
                    >
                      {symbol || (isFocused ? <Cursor /> : null)}
                    </Text>
                  )}
                />
                <AuthButton
                  loading={loading}
                  onPress={() => authCode()}
                  bgColor={styles.grayColor}
                  text="코드 인증하기"
                />
              </AuthContainer>
            </MainContainer>
          </TouchableWithoutFeedback>
          <FloatingButton
            icon="left"
            onPress={() => navigation.goBack()}
            direction="left"
            visible={true}
          />
          <FloatingButton
            icon="right"
            onPress={() => alert("다음 버튼 (연결 x)")}
            direction="right"
            visible={true}
          />
        </KeyboardAvoidingView>
      </FloatingButtonContainer>
    );
  }
};
