import { Platform, Dimensions } from "react-native";
import Constants from "expo-constants";

const { width, height } = Dimensions.get("screen");
//const statusBarHeight =
//  Platform.OS === "android" ? Constants.statusBarHeight : 0;
const statusBarHeight = 0;
const specialPadding = Platform.OS === "adnroid" ? -20 : 0;

export default { width, height, statusBarHeight, specialPadding };
